'use strict';

require('babel-polyfill');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _app = require('../shared/app');

var _app2 = _interopRequireDefault(_app);

var _config = require('../shared/config');

var _reactHotLoader = require('react-hot-loader');

var _redux = require('redux');

var _reactRedux = require('react-redux');

var _store = require('../redux/store');

var _store2 = _interopRequireDefault(_store);

var _reactRouterDom = require('react-router-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var rootEl = document.querySelector(_config.APP_CONTAINER_SELECTOR);

var store = (0, _redux.createStore)(_store2.default,
// eslint-disable-next-line no-underscore-dangle
_config.isProd ? undefined : window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

var wrapApp = function wrapApp(AppComponent, reduxStore) {
  return _react2.default.createElement(
    _reactRedux.Provider,
    { store: reduxStore },
    _react2.default.createElement(
      _reactRouterDom.BrowserRouter,
      null,
      _react2.default.createElement(
        _reactHotLoader.AppContainer,
        null,
        _react2.default.createElement(AppComponent, null)
      )
    )
  );
};

(0, _reactDom.hydrate)(wrapApp(_app2.default, store), rootEl);

if (module.hot) {
  // flow-disable-next-line
  module.hot.accept('../shared/app', function () {
    // eslint-disable-next-line global-require
    var NextApp = require('../shared/app').default;
    (0, _reactDom.hydrate)(wrapApp(NextApp, store), rootEl);
  });
}
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(rootEl, 'rootEl', 'src/client/app.js');
  reactHotLoader.register(store, 'store', 'src/client/app.js');
  reactHotLoader.register(wrapApp, 'wrapApp', 'src/client/app.js');
  leaveModule(module);
})();

;