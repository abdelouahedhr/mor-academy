"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Lsit = function (_Component) {
    _inherits(Lsit, _Component);

    function Lsit(props) {
        _classCallCheck(this, Lsit);

        var _this = _possibleConstructorReturn(this, (Lsit.__proto__ || Object.getPrototypeOf(Lsit)).call(this, props));

        _this.state = {
            value: "",
            drop: "Off",
            active: "Off",
            input: _this
        };

        _this.inputClick = _this.inputClick.bind(_this);
        _this.foucusOut = _this.foucusOut.bind(_this);
        _this.dropClick = _this.dropClick.bind(_this);
        //this.dataClick = this.dataClick.bind(this)
        return _this;
    }

    _createClass(Lsit, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            this.setState({ input: document.getElementById(this.props.id) });
        }
    }, {
        key: "inputClick",
        value: function inputClick(e, callBack) {
            if (this.state.active == "Off") {
                this.setState({ active: "On" });
                this.state.input.focus();
            }
            if (this.state.drop == "Off") {
                this.setState({ drop: "On" });
                this.state.input.focus();
            }
            if (callBack != undefined) {
                callBack();
            }
        }
    }, {
        key: "dropClick",
        value: function dropClick() {
            if (this.state.drop == "Off") {
                this.setState({ drop: "On" });
                this.state.input.focus();
            } else {
                this.setState({ drop: "Off" });
                this.state.input.focusOut();
            }
        }
    }, {
        key: "componentDidUpdate",
        value: function componentDidUpdate() {
            this.state.input.value = this.state.value;
        }
    }, {
        key: "foucusOut",
        value: function foucusOut(e) {
            var _this2 = this;

            setTimeout(function () {
                if (_this2.state.input.value == "") {
                    _this2.setState({ drop: "Off", active: "Off" });
                } else {
                    _this2.setState({ drop: "Off" });
                }
            }, 200);
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            return _react2.default.createElement(
                "div",
                { className: this.props.class },
                _react2.default.createElement("input", {
                    id: this.props.id,
                    className: "default select-input",
                    defaultValue: this.state.value || "",
                    type: "text",
                    name: this.props.name,
                    readOnly: this.props.readOnly,
                    onClick: function onClick(e) {
                        return _this3.inputClick(e);
                    },
                    onBlur: function onBlur(e) {
                        return _this3.foucusOut(e);
                    },
                    onChange: function onChange(e) {
                        return console.log(e);
                    },
                    placeholder: this.props.placeholder
                }),
                _react2.default.createElement("i", { className: "default input-dropdown " + this.props.dropClass, onClick: this.dropClick }),
                _react2.default.createElement(
                    "div",
                    { className: "default dropdown-menu", style: this.state.drop == "Off" ? { visibility: "hidden", height: "135px" } : { visibility: "visible", height: "135px" } },
                    _react2.default.createElement(
                        "svg",
                        {
                            width: "100%",
                            height: "100%",
                            viewBox: "0 0 190 135",
                            preserveAspectRatio: "none" },
                        _react2.default.createElement("path", {
                            className: "svg-dropdown",
                            d: "M95,0 L 100,5 H 185 C185,5 190,5 190,10 V 130 C190,130 190,135 185,135 H 5 C5,135 0,135 0,130 V 10 C0,10 0,5 5,5 H 90 L 95,0 Z",
                            stroke: "none",
                            fill: "white",
                            vectorEffect: "non-scaling-stroke" })
                    )
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Lsit;
}(_react.Component);

var _default = Lsit;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Lsit, "Lsit", "src/shared/components/list.js");
    reactHotLoader.register(_default, "default", "src/shared/components/list.js");
    leaveModule(module);
})();

;