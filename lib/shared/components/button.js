'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = function (_Component) {
    _inherits(Button, _Component);

    function Button(props) {
        _classCallCheck(this, Button);

        var _this = _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).call(this, props));

        _this.state = {
            hovered: 'Off',
            color: _this.props.color,
            type: _this.props.type
        };
        _this.enter = _this.enter.bind(_this);
        _this.leave = _this.leave.bind(_this);
        return _this;
    }

    _createClass(Button, [{
        key: 'enter',
        value: function enter(e) {
            this.setState({ hovered: 'On' });
        }
    }, {
        key: 'leave',
        value: function leave(e) {
            this.setState({ hovered: 'Off' });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            if (this.state.type == 'normal') {
                var style = Object.assign(this.props.style, { backgroundColor: this.state.color, border: '1px solid ' + this.state.color });
                return _react2.default.createElement(
                    'button',
                    { onMouseLeave: function onMouseLeave(e) {
                            return _this2.leave(e);
                        }, onMouseEnter: function onMouseEnter(e) {
                            return _this2.enter(e);
                        }, className: 'button ' + this.props.class, style: _extends({}, style, { filter: this.state.hovered == 'On' ? 'grayscale(15%)' : 'grayscale(0%)' }), value: this.props.value },
                    this.props.value
                );
            } else if (this.state.type == 'empty') {
                var _style = Object.assign(this.props.style, { border: '1px solid ' + this.state.color });
                return _react2.default.createElement(
                    'button',
                    { onMouseLeave: function onMouseLeave(e) {
                            return _this2.leave(e);
                        }, onMouseEnter: function onMouseEnter(e) {
                            return _this2.enter(e);
                        }, className: 'button ' + this.props.class, style: _extends({}, _style, { backgroundColor: this.state.hovered == 'On' ? this.state.color : 'transparent' }), value: this.props.value },
                    this.props.value
                );
            }
        }
    }, {
        key: '__reactstandin__regenerateByEval',
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Button;
}(_react.Component);

var _default = Button;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Button, 'Button', 'src/shared/components/button.js');
    reactHotLoader.register(_default, 'default', 'src/shared/components/button.js');
    leaveModule(module);
})();

;