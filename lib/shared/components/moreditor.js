'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Editor = function (_Component) {
    _inherits(Editor, _Component);

    function Editor(props) {
        _classCallCheck(this, Editor);

        var _this = _possibleConstructorReturn(this, (Editor.__proto__ || Object.getPrototypeOf(Editor)).call(this, props));

        _this.toolbarClick = _this.toolbarClick.bind(_this);
        _this.focus = _this.focus.bind(_this);
        _this.focusout = _this.focusout.bind(_this);
        _this.click = _this.click.bind(_this);
        _this.state = {
            toolbar: ['undo', 'redo', 'separator', 'forecolor', 'separator', 'fontsize', 'fontplus', 'fontminus', 'separator', 'bold', 'italic', 'separator', 'justifyLeft', 'justifyRight', 'justifyCenter', 'justifyFull', 'separator', 'h1', 'h2', 'p', 'separator', 'indent', 'outdent', 'separator', 'insertUnorderedList', 'insertOrderedList', 'separator', 'table', 'separator', 'save'],
            font: false
        };
        return _this;
    }

    _createClass(Editor, [{
        key: 'toolbarClick',
        value: function toolbarClick(e) {
            var command = e.target.getAttribute('name');
            e.preventDefault();
            console.log(command);
            if (command == 'h1' || command == 'h2' || command == 'p') {
                document.execCommand('formatBlock', false, command);
            }

            if (command == 'forecolor' || command == 'backcolor') {
                document.execCommand($(this).data('command'), false, $(this).data('value'));
            }

            if (command == 'createlink' || command == 'insertimage') {
                url = prompt('Enter the link here: ', 'http:\/\/');
                document.execCommand($(this).data('command'), false, url);
            }

            if (command == 'fontplus') {
                var num = Math.min(parseFloat(document.queryCommandValue('FontSize')) + 1, 7).toString();
                document.execCommand('fontSize', false, num);
            }

            if (command == 'fontminus') {
                var num = Math.max(parseFloat(document.queryCommandValue('FontSize')) - 1, 1).toString();
                document.execCommand('fontSize', false, num);
            } else {
                document.execCommand(command, false, null);
            }
        }
    }, {
        key: 'focus',
        value: function focus() {
            this.setState({ font: true });
        }
    }, {
        key: 'focusout',
        value: function focusout() {
            var _this2 = this;

            setTimeout(function () {
                _this2.setState({ font: false });
            }, 100);
        }
    }, {
        key: 'click',
        value: function click(e) {
            e.target.parentNode.parentNode.children[0].value = e.target.getAttribute('value');
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var toolbar = this.state.toolbar.map(function (el, index) {
                if (el == 'separator') {
                    return _react2.default.createElement('div', { key: el + index, className: 'default1 toolbar-separator' });
                }if (el == 'fontsize') {
                    var values = [];
                    for (var i = 6; i < 73; i += 2) {
                        values.push(_react2.default.createElement(
                            'span',
                            { key: i, value: i, className: 'fontvalue default1', onClick: function onClick(e) {
                                    return _this3.click(e);
                                } },
                            i
                        ));
                    }
                    return _react2.default.createElement(
                        'div',
                        { key: el + index, className: 'default1 editortoolbar-button toolbar-fontsize' },
                        _react2.default.createElement('input', { className: 'fontsizevalue', type: 'number', defaultValue: 12, onFocus: _this3.focus, onBlur: _this3.focusout }),
                        _react2.default.createElement(
                            'div',
                            { className: 'default fontsize-container', style: _this3.state.font ? { visibility: 'visible' } : { visibility: 'hidden' } },
                            values
                        )
                    );
                } else {
                    return _react2.default.createElement('div', { key: el + index, name: el, className: 'default1 editortoolbar-button', style: { backgroundImage: "url(./dist/public/assets/icons/editortools/" + el + ".svg)" }, onMouseDown: function onMouseDown(e) {
                            return _this3.toolbarClick(e);
                        } });
                }
            });
            return _react2.default.createElement(
                'div',
                { id: this.props.id, className: 'editor' },
                _react2.default.createElement(
                    'div',
                    { className: 'editor-toolbar' },
                    toolbar
                ),
                _react2.default.createElement('div', { contentEditable: true, className: 'editor-body' })
            );
        }
    }, {
        key: '__reactstandin__regenerateByEval',
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Editor;
}(_react.Component);

var _default = Editor;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Editor, 'Editor', 'src/shared/components/moreditor.js');
    reactHotLoader.register(_default, 'default', 'src/shared/components/moreditor.js');
    leaveModule(module);
})();

;