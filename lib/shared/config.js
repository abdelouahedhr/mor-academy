'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var WEB_PORT = exports.WEB_PORT = process.env.PORT || 8000;
var STATIC_PATH = exports.STATIC_PATH = '/static';
var APP_NAME = exports.APP_NAME = 'Mor academy';

var isProd = exports.isProd = process.env.NODE_ENV === 'production';

var WDS_PORT = exports.WDS_PORT = 7000;

var APP_CONTAINER_CLASS = exports.APP_CONTAINER_CLASS = 'app';
var APP_CONTAINER_SELECTOR = exports.APP_CONTAINER_SELECTOR = '#' + APP_CONTAINER_CLASS;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(WEB_PORT, 'WEB_PORT', 'src/shared/config.js');
  reactHotLoader.register(STATIC_PATH, 'STATIC_PATH', 'src/shared/config.js');
  reactHotLoader.register(APP_NAME, 'APP_NAME', 'src/shared/config.js');
  reactHotLoader.register(isProd, 'isProd', 'src/shared/config.js');
  reactHotLoader.register(WDS_PORT, 'WDS_PORT', 'src/shared/config.js');
  reactHotLoader.register(APP_CONTAINER_CLASS, 'APP_CONTAINER_CLASS', 'src/shared/config.js');
  reactHotLoader.register(APP_CONTAINER_SELECTOR, 'APP_CONTAINER_SELECTOR', 'src/shared/config.js');
  leaveModule(module);
})();

;