"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _courseIntroduction = require("../containers/course-introduction");

var _courseIntroduction2 = _interopRequireDefault(_courseIntroduction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Coursedisp = function (_Component) {
    _inherits(Coursedisp, _Component);

    function Coursedisp(props) {
        _classCallCheck(this, Coursedisp);

        var _this = _possibleConstructorReturn(this, (Coursedisp.__proto__ || Object.getPrototypeOf(Coursedisp)).call(this, props));

        _this.state = {
            chapter: 0
        };
        return _this;
    }

    _createClass(Coursedisp, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { id: "add-cours" },
                _react2.default.createElement(
                    "div",
                    { id: "define-courses" },
                    _react2.default.createElement(_courseIntroduction2.default, null)
                ),
                _react2.default.createElement(
                    "div",
                    { id: "created-chapters" },
                    _react2.default.createElement(
                        "div",
                        { id: "created-chapters-top" },
                        "Chapitrat"
                    )
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Coursedisp;
}(_react.Component);

var _default = Coursedisp;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Coursedisp, "Coursedisp", "src/shared/pages/admin.js");
    reactHotLoader.register(_default, "default", "src/shared/pages/admin.js");
    leaveModule(module);
})();

;