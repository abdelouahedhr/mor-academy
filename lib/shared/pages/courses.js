"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _course = require("../containers/course");

var _course2 = _interopRequireDefault(_course);

var _navBar = require("../containers/nav-bar");

var _navBar2 = _interopRequireDefault(_navBar);

var _appConfig = require("../appConfig.js");

var _appConfig2 = _interopRequireDefault(_appConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Courses = function (_Component) {
    _inherits(Courses, _Component);

    function Courses() {
        _classCallCheck(this, Courses);

        return _possibleConstructorReturn(this, (Courses.__proto__ || Object.getPrototypeOf(Courses)).apply(this, arguments));
    }

    _createClass(Courses, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { className: "courses-container" },
                _react2.default.createElement(
                    "div",
                    { id: "courses-display" },
                    _appConfig2.default.coursestest.map(function (el, index) {
                        return _react2.default.createElement(_course2.default, { key: index, linkUrl: el.url, imgUrl: el.image, title: el.title, duration: el.duration, difficulty: el.difficulty });
                    })
                ),
                _react2.default.createElement(_navBar2.default, null)
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Courses;
}(_react.Component);

var _default = Courses;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Courses, "Courses", "src/shared/pages/courses.js");
    reactHotLoader.register(_default, "default", "src/shared/pages/courses.js");
    leaveModule(module);
})();

;