"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _footer = require("../containers/footer");

var _footer2 = _interopRequireDefault(_footer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Help = function (_Component) {
    _inherits(Help, _Component);

    function Help() {
        _classCallCheck(this, Help);

        return _possibleConstructorReturn(this, (Help.__proto__ || Object.getPrototypeOf(Help)).apply(this, arguments));
    }

    _createClass(Help, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { className: "help-container", style: { backgroundColor: "rgb(248, 248, 248)" } },
                _react2.default.createElement(
                    "h1",
                    { className: "section-title" },
                    "LTA7E9"
                ),
                _react2.default.createElement(
                    "h2",
                    { className: "section-title" },
                    "Had l moubadara maymken liha tenje7 ila blmoussa3ada dialkoum"
                ),
                _react2.default.createElement(
                    "form",
                    { className: "help-form" },
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "Smia :"
                    ),
                    _react2.default.createElement("input", { className: "help-input", name: "first Name", type: "text" }),
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "Lknia :"
                    ),
                    _react2.default.createElement("input", { className: "help-input", name: "Last Name", type: "text" }),
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "Nmira :"
                    ),
                    _react2.default.createElement("input", { className: "help-input", name: "Phone number", type: "tel" }),
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "lmail :"
                    ),
                    _react2.default.createElement("input", { className: "help-input", name: "e-mail", type: "email" }),
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "Fach ghat3awen :"
                    ),
                    _react2.default.createElement("input", { className: "help-input", name: "Phone number", type: "tel" }),
                    _react2.default.createElement(
                        "label",
                        { className: "input-label" },
                        "Chi kalima :"
                    ),
                    _react2.default.createElement("textarea", { className: "textarea", name: "comment" })
                ),
                _react2.default.createElement(_footer2.default, null)
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Help;
}(_react.Component);

var _default = Help;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Help, "Help", "src/shared/pages/help.js");
    reactHotLoader.register(_default, "default", "src/shared/pages/help.js");
    leaveModule(module);
})();

;