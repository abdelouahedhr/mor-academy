"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactYoutube = require("react-youtube");

var _reactYoutube2 = _interopRequireDefault(_reactYoutube);

var _courseNav = require("../containers/course-nav");

var _courseNav2 = _interopRequireDefault(_courseNav);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Coursedisp = function (_Component) {
    _inherits(Coursedisp, _Component);

    function Coursedisp() {
        _classCallCheck(this, Coursedisp);

        return _possibleConstructorReturn(this, (Coursedisp.__proto__ || Object.getPrototypeOf(Coursedisp)).apply(this, arguments));
    }

    _createClass(Coursedisp, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { id: "course-display" },
                _react2.default.createElement(
                    "div",
                    { id: "course-body" },
                    _react2.default.createElement("div", { className: "course-domaine-bg" }),
                    _react2.default.createElement(
                        "div",
                        { id: "course-video-container" },
                        _react2.default.createElement(
                            "h1",
                            { className: "chapter-title" },
                            "Besmellah!!"
                        ),
                        _react2.default.createElement(_reactYoutube2.default, { videoId: "79nucpqjA2k", opts: { height: "100%", width: "100%", autoplay: 1 }, containerClassName: "youtube-video-container" })
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { id: "course-text-display" },
                    _react2.default.createElement("div", { id: "course-text" })
                ),
                _react2.default.createElement(_courseNav2.default, null)
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Coursedisp;
}(_react.Component);

var _default = Coursedisp;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Coursedisp, "Coursedisp", "src/shared/pages/course.js");
    reactHotLoader.register(_default, "default", "src/shared/pages/course.js");
    leaveModule(module);
})();

;