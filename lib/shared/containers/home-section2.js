"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _button = require("../components/button");

var _button2 = _interopRequireDefault(_button);

var _comp = require("../components/comp");

var _comp2 = _interopRequireDefault(_comp);

var _appConfig = require("../appConfig.js");

var _appConfig2 = _interopRequireDefault(_appConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var How = function (_Component) {
    _inherits(How, _Component);

    function How() {
        _classCallCheck(this, How);

        return _possibleConstructorReturn(this, (How.__proto__ || Object.getPrototypeOf(How)).apply(this, arguments));
    }

    _createClass(How, [{
        key: "render",
        value: function render() {
            var howComponents = _appConfig2.default.how.map(function (el) {
                return _react2.default.createElement(_comp2.default, { key: el.image, type: "how", image: el.image, text: el.text, "class": "how-component" });
            });
            return _react2.default.createElement(
                "div",
                { className: "home-section" },
                _react2.default.createElement(
                    "h1",
                    { className: "section-title" },
                    "KIFACH?"
                ),
                _react2.default.createElement(
                    "div",
                    { className: "how-container" },
                    howComponents
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return How;
}(_react.Component);

var _default = How;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(How, "How", "src/shared/containers/home-section2.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/home-section2.js");
    leaveModule(module);
})();

;