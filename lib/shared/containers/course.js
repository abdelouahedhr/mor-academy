"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require("react-router-dom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Course = function (_Component) {
    _inherits(Course, _Component);

    function Course(props) {
        _classCallCheck(this, Course);

        var _this = _possibleConstructorReturn(this, (Course.__proto__ || Object.getPrototypeOf(Course)).call(this, props));

        _this.click = _this.click.bind(_this);
        return _this;
    }

    _createClass(Course, [{
        key: "click",
        value: function click(e) {
            this.props.history.push("/Courses/" + this.props.linkUrl);
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                "div",
                { className: "course" },
                _react2.default.createElement("img", { onClick: function onClick(e) {
                        _this2.click(e);
                    }, className: "course-image", src: "./dist/public/assets/coursesImages/" + this.props.imgUrl + ".jpg" }),
                _react2.default.createElement(
                    "span",
                    { onClick: function onClick(e) {
                            _this2.click(e);
                        }, className: "course-title" },
                    this.props.title
                ),
                _react2.default.createElement(
                    "div",
                    { className: "course-info" },
                    _react2.default.createElement(
                        "div",
                        { className: "course-duration course-info-element" },
                        this.props.duration
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "course-difficulty course-info-element" },
                        this.props.difficulty
                    ),
                    _react2.default.createElement("input", { type: "checkbox", className: "course-save-later course-info-element" })
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Course;
}(_react.Component);

var _default = (0, _reactRouterDom.withRouter)(Course);

exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Course, "Course", "src/shared/containers/course.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/course.js");
    leaveModule(module);
})();

;