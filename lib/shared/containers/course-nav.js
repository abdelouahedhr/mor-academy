"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Coursenav = function (_Component) {
    _inherits(Coursenav, _Component);

    function Coursenav() {
        _classCallCheck(this, Coursenav);

        return _possibleConstructorReturn(this, (Coursenav.__proto__ || Object.getPrototypeOf(Coursenav)).apply(this, arguments));
    }

    _createClass(Coursenav, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                { id: "course-nav" },
                _react2.default.createElement(
                    "div",
                    { className: "course-author" },
                    _react2.default.createElement("img", { className: "course-author-image", src: "" }),
                    _react2.default.createElement(
                        "div",
                        { className: "course-author-inf" },
                        _react2.default.createElement("span", { className: "course-author-name" }),
                        _react2.default.createElement("span", { className: "course-author-function" })
                    ),
                    _react2.default.createElement("div", { className: "course-chapters-display" })
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Coursenav;
}(_react.Component);

var _default = Coursenav;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Coursenav, "Coursenav", "src/shared/containers/course-nav.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/course-nav.js");
    leaveModule(module);
})();

;