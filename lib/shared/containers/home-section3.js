"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require("react-router-dom");

var _button = require("../components/button");

var _button2 = _interopRequireDefault(_button);

var _comp = require("../components/comp");

var _comp2 = _interopRequireDefault(_comp);

var _appConfig = require("../appConfig.js");

var _appConfig2 = _interopRequireDefault(_appConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Why = function (_Component) {
    _inherits(Why, _Component);

    function Why() {
        _classCallCheck(this, Why);

        return _possibleConstructorReturn(this, (Why.__proto__ || Object.getPrototypeOf(Why)).apply(this, arguments));
    }

    _createClass(Why, [{
        key: "render",
        value: function render() {
            var whyComponents = _appConfig2.default.why.map(function (el) {
                return _react2.default.createElement(_comp2.default, { key: el.image, type: "why", image: el.image, text: el.text, "class": "why-component" });
            });
            return _react2.default.createElement(
                "div",
                { className: "why-section" },
                _react2.default.createElement(
                    "h1",
                    { className: "section-title" },
                    "3LACH?"
                ),
                _react2.default.createElement(
                    "div",
                    { className: "why-container" },
                    _react2.default.createElement(
                        "h2",
                        { className: "section-title" },
                        "Hadi wa7ed l moubadara khayria li hadaf menha houwa nwefrou dorouss f lmoustawa, fabour o b darija (o l amazighia nchaelah) bach nsehlou ta3aloum 3la chabab lmaghribi"
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "why-container", style: { backgroundColor: "#f8f8f8", overflow: "hidden" } },
                        whyComponents
                    ),
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { to: "/Help", className: "help-us" },
                        "\u2192\u20033awna"
                    )
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Why;
}(_react.Component);

var _default = Why;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Why, "Why", "src/shared/containers/home-section3.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/home-section3.js");
    leaveModule(module);
})();

;