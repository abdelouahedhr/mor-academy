"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _button = require("../components/button");

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Bg = function (_Component) {
    _inherits(Bg, _Component);

    function Bg() {
        _classCallCheck(this, Bg);

        return _possibleConstructorReturn(this, (Bg.__proto__ || Object.getPrototypeOf(Bg)).apply(this, arguments));
    }

    _createClass(Bg, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                null,
                _react2.default.createElement("img", { className: "home-bg", src: "./dist/public/assets/bg.svg" }),
                _react2.default.createElement(
                    "h1",
                    { className: "title" },
                    "dir 3lach twelli a khay diali"
                ),
                _react2.default.createElement(
                    "h2",
                    { className: "sub-title" },
                    "courat b darija o fabouuur"
                ),
                _react2.default.createElement(_button2.default, { type: "empty", color: "#20cb97", value: "M9EYED", "class": "sign-in", style: {} }),
                _react2.default.createElement(_button2.default, { type: "normal", color: "#ff6364", value: "LA!! DOUZ T9EYED", "class": "sign-up", style: {} })
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Bg;
}(_react.Component);

var _default = Bg;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Bg, "Bg", "src/shared/containers/home-section1.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/home-section1.js");
    leaveModule(module);
})();

;