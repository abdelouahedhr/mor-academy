"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _list = require("../components/list");

var _list2 = _interopRequireDefault(_list);

var _moreditor = require("../components/moreditor");

var _moreditor2 = _interopRequireDefault(_moreditor);

var _store = require("../../redux/store");

var _store2 = _interopRequireDefault(_store);

var _syncActions = require("../../redux/syncActions");

var _syncActions2 = _interopRequireDefault(_syncActions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Courseintro = function (_Component) {
    _inherits(Courseintro, _Component);

    function Courseintro(props) {
        _classCallCheck(this, Courseintro);

        var _this = _possibleConstructorReturn(this, (Courseintro.__proto__ || Object.getPrototypeOf(Courseintro)).call(this, props));

        _this.click = _this.click.bind(_this);
        return _this;
    }

    _createClass(Courseintro, [{
        key: "click",
        value: function click(e) {
            var childs = document.getElementById("course-intro").children,
                chapter = {};
            Array.prototype.forEach.call(childs, function (child) {
                if (child.tagName == "INPUT") {
                    chapter[child.name] = child.value;
                } else if (child.tagName == "DIV" && child.className != "editor") {
                    chapter[child.children[0].name] = child.children[0].value;
                } else if (child.tagName != "BUTTON") {
                    chapter["body"] = child.children[1].innerHTML;
                }
                console.log(chapter);
                _store2.default.dispatch((0, _syncActions2.default)(chapter));
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                "div",
                { id: "course-intro" },
                _react2.default.createElement("input", { className: "addcourse-input navbar-elem input", type: "text", name: "title", placeholder: "Titre?" }),
                _react2.default.createElement(_list2.default, { "class": "addcourse-input navbar-elem", id: "cours-domaine", dropClass: "fas fa-arrow-circle-down", readOnly: "true", name: "domaine", placeholder: "informatik, lmath, lphisic..." }),
                _react2.default.createElement(_list2.default, { "class": "addcourse-input navbar-elem", id: "cours-difficulty", dropClass: "fas fa-arrow-circle-down", readOnly: "true", name: "difficulty", placeholder: "S3oubia" }),
                _react2.default.createElement("input", { className: "addcourse-input navbar-elem input", type: "text", name: "duration", placeholder: "Lwe9t?" }),
                _react2.default.createElement("input", { className: "addcourse-input navbar-elem input", type: "text", name: "urlId", placeholder: "Lvideo dial l mo9edima?" }),
                _react2.default.createElement(_moreditor2.default, null),
                _react2.default.createElement(
                    "button",
                    { className: "validate-chapter-button", value: "Salit", onClick: function onClick(e) {
                            return _this2.click(e);
                        } },
                    "Salit"
                )
            );
        }
    }, {
        key: "__reactstandin__regenerateByEval",
        value: function __reactstandin__regenerateByEval(key, code) {
            this[key] = eval(code);
        }
    }]);

    return Courseintro;
}(_react.Component);

var _default = Courseintro;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(Courseintro, "Courseintro", "src/shared/containers/course-introduction.js");
    reactHotLoader.register(_default, "default", "src/shared/containers/course-introduction.js");
    leaveModule(module);
})();

;