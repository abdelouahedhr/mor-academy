"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _home = require("../pages/home");

var _home2 = _interopRequireDefault(_home);

var _help = require("../pages/help");

var _help2 = _interopRequireDefault(_help);

var _courses = require("../pages/courses");

var _courses2 = _interopRequireDefault(_courses);

var _course = require("../pages/course");

var _course2 = _interopRequireDefault(_course);

var _admin = require("../pages/admin");

var _admin2 = _interopRequireDefault(_admin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

var routes = [{
    path: "/",
    exact: true,
    component: _home2.default
}, {
    path: "/Home",
    exact: false,
    component: _home2.default
}, {
    path: "/Courses",
    exact: true,
    component: _courses2.default
}, {
    path: "/Help",
    exact: false,
    component: _help2.default
}, {
    path: "/Courses/:id",
    exact: true,
    component: _course2.default
}, {
    path: "/admin",
    exact: false,
    component: _admin2.default
}];

var _default = routes;
exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(routes, "routes", "src/shared/routes/routes.js");
    reactHotLoader.register(_default, "default", "src/shared/routes/routes.js");
    leaveModule(module);
})();

;