'use strict';

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _server = require('react-dom/server');

var _server2 = _interopRequireDefault(_server);

var _reactDom = require('react-dom');

var _reactRouter = require('react-router');

var _redux = require('redux');

var _reactRedux = require('react-redux');

var _config = require('../shared/config');

var _app = require('../shared/app');

var _app2 = _interopRequireDefault(_app);

var _renderApp = require('./render-app');

var _renderApp2 = _interopRequireDefault(_renderApp);

var _store = require('../redux/store');

var _store2 = _interopRequireDefault(_store);

var _routes = require('../shared/routes/routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var app = (0, _express2.default)();

app.use((0, _compression2.default)());
app.use(_config.STATIC_PATH, _express2.default.static('dist'));
app.use(_config.STATIC_PATH, _express2.default.static('public'));

app.get('/', function (req, res) {
  try {
    var comp = _routes2.default.find(function (route) {
      return (0, _reactRouter.matchPath)(req.path, route) != null;
    });

    if (comp != undefined) {
      var context = {},
          store = (0, _redux.createStore)(_store2.default, {});
      var html = _server2.default.renderToString(_react2.default.createElement(
        _reactRedux.Provider,
        { store: store },
        _react2.default.createElement(
          _reactRouter.StaticRouter,
          { context: context, location: req.path },
          _react2.default.createElement(_app2.default, { context: context })
        )
      ));
      res.send((0, _renderApp2.default)(html, "", store));
    } else {
      res.end();
    }
  } catch (error) {
    console.log(error);
    res.status(400).send((0, _renderApp2.default)('An error occured.'));
  }
});

app.listen(_config.WEB_PORT, function () {
  // eslint-disable-next-line no-console
  console.log('Server running on port ' + _config.WEB_PORT + ' ' + (_config.isProd ? '(production)' : '(development).\nKeep "npm run dev:wds" running in an other terminal') + '.');
});
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(app, 'app', 'src/server/index.js');
  leaveModule(module);
})();

;