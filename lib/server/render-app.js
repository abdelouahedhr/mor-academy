'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../shared/config');

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

var renderFullPage = function renderFullPage(html, css, st) {
    return '<!DOCTYPE html>\n<html>\n    <head>\n        <title> ' + _config.APP_NAME + ' </title>\n        <link rel="stylesheet" href="/' + _config.STATIC_PATH + '/css/styles.css">\n        <style>\n            ' + css + '\n        </style>\n    </head>\n    <body >\n        <div id="' + _config.APP_CONTAINER_CLASS + '">\n            ' + html + '\n        </div>\n        <script>\n            window.__PRELOADED_STATE__ = ' + JSON.stringify(st.getState()) + '\n        </script>\n        <script src="' + (_config.isProd ? _config.STATIC_PATH : 'http://localhost:' + _config.WDS_PORT + '/dist') + '/js/bundle.js"></script>\n    </body>\n</html>';
};

var _default = renderFullPage;
exports.default = _default;

//

;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(renderFullPage, 'renderFullPage', 'src/server/render-app.js');
    reactHotLoader.register(_default, 'default', 'src/server/render-app.js');
    leaveModule(module);
})();

;