'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = require('redux');

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

function chaptersReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments[1];

    switch (action.type) {
        case 'ADD_CHAPTER':
            return state.concat(action.chapter);
    }
    return state;
}

function coursesReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments[1];

    switch (action.type) {
        case 'FILTER_COURSES':
            return state.concat(action.filter);
    }
    return state;
}

var _default = (0, _redux.combineReducers)({
    chapters: chaptersReducer,
    courses: coursesReducer
});

exports.default = _default;
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(chaptersReducer, 'chaptersReducer', 'src/redux/store.js');
    reactHotLoader.register(coursesReducer, 'coursesReducer', 'src/redux/store.js');
    reactHotLoader.register(_default, 'default', 'src/redux/store.js');
    leaveModule(module);
})();

;