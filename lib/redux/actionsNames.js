"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var ADD_CHAPTER = exports.ADD_CHAPTER = "ADD_CHAPTER";
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ADD_CHAPTER, "ADD_CHAPTER", "src/redux/actionsNames.js");
  leaveModule(module);
})();

;