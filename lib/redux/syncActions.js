"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addChapter = undefined;

var _actionsNames = require("./actionsNames");

(function () {
    var enterModule = require('react-hot-loader').enterModule;

    enterModule && enterModule(module);
})();

var addChapter = exports.addChapter = function addChapter(chapter) {
    return {
        type: _actionsNames.ADD_CHAPTER,
        chapter: chapter
    };
};
;

(function () {
    var reactHotLoader = require('react-hot-loader').default;

    var leaveModule = require('react-hot-loader').leaveModule;

    if (!reactHotLoader) {
        return;
    }

    reactHotLoader.register(addChapter, "addChapter", "src/redux/syncActions.js");
    leaveModule(module);
})();

;