module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        watch: {
            scssConcat: {
              files: ["cssSrc/sass/**/*.scss"],
              tasks: ["concat:compass1"],
            },
            scssMixinsConcat: {
                files: ["cssSrc/scss/*.scss"],
                tasks: ["concat:compass2"],
              },
            cssPost: {
                files: ["cssSrc/styles.css"],
                tasks: ["postcss"]
              },
            cssSass : {
                files: ["cssSrc/css/*.scss"],
                tasks: ["sass"],
              },
        },
        concat: {
          compass1 : {
            src: ["cssSrc/sass/fonts/fonts.scss","cssSrc/sass/**/*.scss"],
            dest: "cssSrc/scss/styles.scss",
          },
          compass2 : {
                src: ["cssSrc/scss/css3-mixins.scss","cssSrc/scss/styles.scss"],
                dest: "cssSrc/css/styles.scss"
            }
        },
        sass: {                  // Task
            dist: {
                files: {
                  "cssSrc/styles.css": "cssSrc/css/styles.scss"
                }
              }
          },    
        postcss: {
            options: {
              processors: [ // add fallbacks for rem units
                require("cssnano")() // minify the result
              ]
            },
            dist: {
              src: "cssSrc/styles.css",
              dest: "static/css/styles.css",
            }
          }
      });

      grunt.loadNpmTasks("grunt-contrib-watch");
      grunt.loadNpmTasks("grunt-contrib-concat");
      grunt.loadNpmTasks("grunt-postcss");
      grunt.loadNpmTasks("grunt-contrib-sass");

      grunt.registerTask("default", ["watch"]);
  };