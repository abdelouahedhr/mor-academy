import path from "path"
import webpack from "webpack"

import { WDS_PORT, isProd } from "./src/shared/config"

export default {
  entry: {
      app : [
          "react-hot-loader/patch",
          "./src/client/app.js"
        ]
  },
  output: {
    filename: "js/bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: isProd ? "/static/" : `http://localhost:${WDS_PORT}/dist/`,
  },
  module: {
    rules: [
            {
                test: /\.(js|jsx)$/,
                use:  "babel-loader",
                exclude: /node_modules/
            },
            {
              test: /\.css$/,
              use: [
                  "style-loader",
                  {
                      loader: "css-loader",
                      options: {
                          importLoaders: 1
                      }
                  },
                  {
                      loader : "postcss-loader",
                      options: {
                          config: {
                            path: "./postcss.config.js"
                          }
                        }
                  }
              ]
          },
            {
                test: /\.svg/,
                    use: {
                        loader: 'svg-url-loader',
                        options: {}
                    }
            }
    ],
  },
  devtool: isProd ? false : "source-map",
  resolve: {
    extensions: [".js", ".jsx"],
  },
  devServer: {
    port: WDS_PORT,
    hot: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
    }
  },
  mode : isProd ? "production" : "development",

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ]
}
