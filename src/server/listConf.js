module.exports.list = [
  {
    url : "getPostes",
    collection : "postes",
    fields : ["poste"]
  },
  {
    url : "getUsers",
    collection : "users",
    fields : ["username","poste"]

  },
  {
    url : "getDifficulty",
    collection : "difficulty",
    fields : ["name","_id"]
  },
  {
    url : "getProfile",
    collection : "userType",
    fields : ["name","_id"]
  },
  {
    collection : "domaines",
    fields : ["name","_id","description"],
    url : "getDomaines"
  }
];

module.exports.DbConf = {
  url : 'mongodb://localhost/university'
}

module.exports.listCRUD = [
  {
    url : "getDifficulty",
    collection : "difficulty",
    fields : ["name","_id"]
  },
  {
    url : "getProfile",
    collection : "userType",
    fields : ["name","_id"]
  },
  {
    collection : "domaines",
    fields : ["name","_id"],
    url : "getDomaines"
  }
]