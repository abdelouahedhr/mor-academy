import compression from 'compression'
import helmet from 'helmet'
import React from 'react'
import ReactDOM from 'react-dom/server'
import { StaticRouter as Router, matchPath } from 'react-router'
import { createStore} from 'redux'
import { Provider } from 'react-redux'
import { STATIC_PATH, WEB_PORT, isProd } from '../shared/config'
import App from '../shared/app'
import renderApp from './render-app'
import reducers from '../redux/store'
import routes from '../shared/routes/routes'

// server imports : 
var express = require('express')
var courses_router = require('./routers/courses')
var users_router = require('./routers/users')
var lists_router = require('./routers/lists')
var mongoose = require('mongoose')
var body_parser = require('body-parser')
var passport = require("passport")
var cookieParser = require("cookie-parser")
var session = require("express-session")
var passport_setup = require("./passport-setup")
var cors = require('cors')
var conf = require('./listConf')
// end server import.



// server back-end routes : 
var app = express()
app.use(cors())

// connect to data-base : 
var DbUrl = conf.DbConf.url
mongoose.connect(DbUrl);
passport_setup();




app.use(body_parser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
		secret: "TKRv0IJs=HYqrvagQ#&!F!%V]Ww/4KiVs$s,<<MX",
		resave: true,
    saveUninitialized: true,
    maxAge : 300000
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.json())

app.use(compression())
app.use(helmet())
app.use(STATIC_PATH, express.static('dist'))
app.use(STATIC_PATH, express.static('./static'))


// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res, next) {
  var response = req.date_time
  next()
})


app.get('/gen_random/:min-:max', function (req,res,next){
   var min = parseInt(req.params.min)
   var max = parseInt(req.params.max)
   
   if (isNaN(min) || isNaN(max)) {
       res.status(400)
       res.json({ err : 'Bad params'})
       return;
   }
   
   var result = Math.round((Math.random() * (max - min)) + min);
   res.json({ result: result });

})

app.use('/courses',courses_router)
app.use('/users',users_router)
app.use('/list',lists_router)
// end server back routes.




// for isomorphic server rendering : 
app.get('*', async (req, res) => {
  try {
    let comp = routes.find((route)=>{
        return matchPath(req.path,route) != null          
    })
    
    if(comp != undefined){
      const context = {} , store = createStore(reducers, {})
      const html = ReactDOM.renderToString(
        <Provider store={store}>
            <Router context={context} location={req.path}>
                <App context={context} />
            </Router>
        </Provider>)
      res.send(renderApp(html,store))
    }else{
      res.end()
    }
    
  } catch (error) {
    console.log(error)
    res.status(400).send(renderApp('An error occured.'));
  }
})

app.listen(WEB_PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port ${WEB_PORT} ${isProd ? '(production)' :
  '(development).\nKeep "npm run dev:wds" running in an other terminal'}.`)
})
