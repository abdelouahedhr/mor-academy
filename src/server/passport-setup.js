var passport = require("passport");
var User = require("./models/user");
var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;


module.exports = function() {

		passport.serializeUser(function(user, done) {
			done(null, user._id);
		});
		passport.deserializeUser(function(id, done) {
			User.findById(id, function(err, user) {
				done(err, user);
			});
		});
};
passport.use("login", new LocalStrategy(

	function(username, password, done) {
		User.findOne({ username: username }, function(err, user) {
			if (err) { return done(err); }
			if (!user) {
				return done(null, false,
			 	{ message: "Utilisateur inexistant !!"});
			}
			// check if user is verified :
			if(!user.verified){
				return done(null, false,
			 	{ message: "user not yet comfirmed !!!" });
			}
			user.checkPassword(password, function(err, isMatch) {
				if (err) { return done(err); }
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false,
				 	{ message: "Utilisateur inexistant !!" });
				}
			});
		});
}));

passport.use(new FacebookStrategy({
    clientID: '688452334686211',
    clientSecret: '4b23752502f43781f86b512dfea9addd',
    callbackURL: "http://localhost:8000/users/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
        console.log(profile);
		var me = new User({});
		me.username    = profile.username;
		me.displayName = profile.displayName;

        /* save if new */
        User.findOne({username:me.username}, function(err, u) {
            if(!u) {
                me.save(function(err, me) {
                    if(err) return done(err);
                    done(null,me);
                });
            } else {
                console.log(u);
                done(null, u);
            }
        });
  }
));