var Course = require('../models/course');

exports.postCourses = function(req, res,next) {

  if(!req.isAuthenticated()){
    //req.isAuthenticated() will return true if user is logged in
    res.status
    var json_return = {}
    json_return.msg = 'Acces denied redirect to login !!'
    res.status(401)
    res.json(json_return).end()
    return
  }

  var course = new Course();
  course.name = req.body.name;
  course.type = req.body.type;
  console.log("user in the session : "+req.user)
  course.author =  req.user._id;
  //get the course author from req object and insert it s ID in course DOC.


  course.save(function(err) {
    if (err)
      res.send(err);
    res.json({ message: 'course added!', data: course });
  });
};

exports.getCourses = function(req, res) {
  Course.find(function(err, courses) {
    if (err)
      res.send(err);

    res.json(courses);
  });
};

exports.getCourse = function(req, res) {
  Course.findById(req.params.course_id, function(err, course) {
    if (err)
      res.send(err);

    res.json(course);
  });
};

exports.putCourse = function(req, res) {

  Course.findById(req.params.beer_id, function(err, course) {
    if (err)
      res.send(err);

    course.save(function(err) {
      if (err)
        res.send(err);

      res.json(course);
    });
  });
};

exports.deleteCourse = function(req, res) {
  Course.findByIdAndRemove(req.params.course_id, function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Course removed from data base!' });
  });
};