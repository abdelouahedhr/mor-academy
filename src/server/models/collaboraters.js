// Load required packages
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CollaboratorSchema   = new mongoose.Schema({
  name:     String,
  lastName : String,
  phone : String,
  email : { type: String, required: true, unique: true },
  age : Number,
  profile : {type: Schema.Types.ObjectId, ref: 'Profile'} 
});
// Export the Mongoose model
module.exports = mongoose.model('Collaborator', CollaboratorSchema);