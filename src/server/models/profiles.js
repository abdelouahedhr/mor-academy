// Load required packages
var mongoose = require('mongoose');
var User = require('../models/user');
var Schema = mongoose.Schema;

var ProfileSchema   = new mongoose.Schema({
  name:     String,
  default : { type : Boolean, default : false}
});
// Export the Mongoose model
module.exports = mongoose.model('Profile', ProfileSchema);