// Load required packages
var mongoose = require('mongoose');
var User = require('../models/user');
var Schema = mongoose.Schema;

var CourseSchema   = new mongoose.Schema({
  name: String,
  type: String,
  imageUrl : String,
  author : {type: Schema.Types.ObjectId, ref: 'User'},
  followers : [{ type: Schema.Types.ObjectId, ref: 'User' }],
  numberOfFollowers : Number,
  numberOfLike : Number,
  chapters : [{
    title : String,
    chapter_id : Number,
    text : String,
    video_link : String
  }]
});

// Export the Mongoose model
module.exports = mongoose.model('Course', CourseSchema);