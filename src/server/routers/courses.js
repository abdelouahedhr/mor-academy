var express = require('express')
var router = express.Router()
var courseController = require('../controllers/course')



router.route('/Courses')
  .post(courseController.postCourses)
  .get(courseController.getCourses);


router.route('/courses/:course_id')
  .get(courseController.getCourse)
  .put(courseController.putCourse)
  .delete(courseController.deleteCourse);


module.exports = router