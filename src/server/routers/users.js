var express = require('express')
var router = express.Router()
var User = require('../models/user')
var passport = require("passport");
var randomString = require("randomstring")
var nodemailer = require("nodemailer")


// create transporter for email confirmation sending : 
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'mor.academy.learning@gmail.com', 
	pass: 'aqwzsx+-123456'
  }
});

// route to handle sign-up confirmation : 
router.get('/:token', function(req,res,next){

	var token = req.params.token;
    var query = {token : token, verified : false}
    var json_return = {}
	User.findOneAndUpdate(query, {verified : true}, function(err,resp){
          if(err){
          	console.log('error updating user '+err)
          	json_return.msg = 'Error retrieving data please try again !!'
          	res.status(500)
          	res.json(json_return).end()
          	return;
          }else if(resp){
            
            console.log('updated user : ' + resp)
            json_return.msg = 'user verified with success : redirect to /login'
            res.status(200)
			//res.json(json_return).end()
			res.redirect("/Sign")
          	return;

          }else{
          	console.log('return from findOneAndUpdate: ' + resp)
          	json_return.msg = 'no user founded or already verified !!'
            res.status(200)
          	res.json(json_return).end()
            return;

          }
	})
})



router.get('/', function(req,res,next){

	User.find().exec(function(err,users){
       var json = {}
       if (err) {
       	    
       	    json.err = err
       	    res.status(403)
         	res.json(json)
         	return
       }
       json.msg = users
       res.status(200)
       res.json(json)
       res.end()
	})
})

router.post('/sign-up',function(req,res,next){
    
    console.log('request body : '+ req.body.username)
	var username = req.body.username
    var password = req.body.password
    console.log('params : '+ username +' '+password)
    var json = {}
    if(!username || !password){
    	res.status(403)
    	json.err = 'Bad request!!'
    	res.json(json).end()
    	return
    }
    
    // checker if the username exist in the data base : 
    User.findOne({username : username}, function(err,user){
    	
    	if(err){
            json.err = err
            res.status(403)
            res.json(json).end()
            return
    	}

    	if(user){
    		json.err   = "User already exist !!"
    		res.status(203)
    		res.json(json).end()
    		return
    	}
    	var user = new User({ username : username,password : password })
    	console.log('user to insert '+ user)

    	// generate random string and add it to the user :
    	var token = randomString.generate({
           length : 64
    	});

    	user.token = token;

    	user.save(function(err){
    		if(err) {

    			json.err = 'Bad request!!'
    			res.status(500)
   
    	        res.json(json).end()
    		}
    		res.status(200)
    		res.json({ msg : 'user created with success and mail confirmation sended !!'}).end()

    		var mailOptions = {
			  from: 'youssef.aithaddouch@gmail.com',
			  to: username,
			  subject: 'Email de comfirmation MOR-ACADEMY',
			  text: 'Bonjour, merci de comfirmer votre sign-up en cliquant sur le lien :  http://localhost:8000/users/' + token 
			};

    		//send the email with link confirmation : (optimize later with template and config file)
    		transporter.sendMail(mailOptions, function(error, info){
			  if (error) {
			    console.log(error);
			  } else {
			    console.log('Email sent: ' + info.response);
			  }
			});

			
    	})

    })

})

router.post('/collaborater',function(req,res){
      
})

// for facebook authentication :
router.get('/auth/facebook', passport.authenticate('facebook'));

router.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect: '/',
                                      failureRedirect: '/login' }));



router.post('/login',function(req,res,next){
    console.log("request to login handler : !!!!")
	passport.authenticate("login",function(err,user,msg){
        var json = {}
		if (err) {
			console.log('Error in handling request :' + err)
			json.msg = 'Error in server try later'
			res.status(500)
			res.json(json).end()
			return;
		}
	    if (!user) { 
	    	console.log('wrong credentiels !!')
	    	json.msg = msg.message
	    	res.status(201)
	    	res.json(json).end()
	    	return;
	    }
	    req.logIn(user, function(err) {
		    if (err) { 
				console.log("user in the session : "+req.user)
		      	json.msg = 'Error in server try later'
				res.status(500)
				res.json(json).end()
				return; 
			}
			console.log("user in the session : "+req.user)
			json.msg = 'Authetication with success!!'
			var profile = {}
			profile.username    = user.username
			profile.createdAt   = user.createdAt
			profile.displayName = user.displayName
			profile.displayImg  = user.displayImg
			json.profile = profile
	    	res.status(200)
	    	res.json(json).end()
	    	return;
	    });

    })(req, res, next);
})






module.exports = router













