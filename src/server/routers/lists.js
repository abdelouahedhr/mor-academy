var express = require('express')
var router = express.Router()
var mongoose  = require('mongoose')
var listConf = require('../listConf');
var listConfArray     = listConf.list;
var listConfCrudArray = listConf.listCRUD;
var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
var conf = require('../listConf')
var DbUrl = conf.DbConf.url
// for updating a list : 
router.post("/listCRUD/:idList",function(req,res){
  var idList = req.params.idList;
  var response = {}
  var exist = false
  console.log('id list to get from conf' + idList)
  var listConfObject = {}
  // search for this id in listConf FILE :
  for (let index = 0; index < listConfCrudArray.length; index++) {
    console.log('current url '+ listConfCrudArray[index].url )
    if (listConfCrudArray[index].url == idList){
      listConfObject = listConfCrudArray[index];
      exist = true
      break;
    }
  }
  // if conf not found the return req with data not found : 
  if (!exist){
     res.status(201);
     response.msg = 'no config founded for requested data';
     res.json(response);
     return;
  }
  // else request data base for data : 
  // get collection name : 
  var collectionName = listConfObject.collection
  console.log('collection ' + collectionName)
  var insert = req.body;
  var query = {}
  query.name = insert.name;
  MongoClient.connect(DbUrl, function(err, db) {
    if(err) { 
      console.dir(err);
      res.status(500);
      response.msg = 'Error in server try later';
      res.json(response);
      }

    var collection = db.collection(collectionName);
    collection.findOne(query, function(err, result) {
      if (err){
            console.dir(err);
            res.status(501);
            response.msg = 'Cannot find data, try again or contact your admin';
            res.json(response);
      }
      if(result){
            console.log('element already exist '+result);
            res.status(501);
            response.msg = 'data already exist, please update it if you want !!';
            res.json(response).end();
            return;
      }
      collection.insert(insert,function(err, success) {
        if(err){
          console.dir(err);
          res.status(501);
          response.msg = 'Cannot find data, try again or contact your admin';
          res.json(response);
        }
        res.status(200);
        response.msg = 'Added with success !!';
        res.json(response).end();
      });  
      db.close();
    });    
  });
})
router.put('/listCRUD/:idList',function(req,res){
  var idList = req.params.idList;
  var response = {}
  var exist = false
  console.log('id list to get from conf' + idList)
  var listConfObject = {}
  // search for this id in listConf FILE :
  for (let index = 0; index < listConfCrudArray.length; index++) {
    console.log('current url '+ listConfCrudArray[index].url )
    if (listConfCrudArray[index].url == idList){
      listConfObject = listConfCrudArray[index];
      exist = true
      break;
    }
  }
  // if conf not found the return req with data not found : 
  if (!exist){
     res.status(201);
     response.msg = 'no config founded for requested data';
     res.json(response);
     return;
  }
  // else request data base for data : 
  // get collection name : 
  var collectionName = listConfObject.collection
  console.log('collection ' + collectionName)
  var insert = req.body;
  var query = {}
  var o_id = new mongo.ObjectID(insert._id);
  query._id = o_id;
  insert._id = o_id;
  MongoClient.connect(DbUrl, function(err, db) {
    if(err) { 
      console.dir(err);
      res.status(500);
      response.msg = 'Error in server try later';
      res.json(response);
      }

    var collection = db.collection(collectionName);
    collection.updateOne(query, insert, function(err, success) {
      if (err){
            console.dir(err);
            res.status(501);
            response.msg = 'Cannot find data, try again or contact your admin';
            res.json(response).end();
            return;
      }
      res.status(200);
      response.msg = 'data updated with success !!';
      res.json(response).end();
      db.close();
    });    
  });
})

router.delete('/listCRUD/:idList',function(req,res){
  var idList = req.params.idList;
  var response = {}
  var exist = false
  console.log('id list to get from conf' + idList)
  var listConfObject = {}
  // search for this id in listConf FILE :
  for (let index = 0; index < listConfCrudArray.length; index++) {
    console.log('current url '+ listConfCrudArray[index].url )
    if (listConfCrudArray[index].url == idList){
      listConfObject = listConfCrudArray[index];
      exist = true
      break;
    }
  }
  // if conf not found the return req with data not found : 
  if (!exist){
     res.status(201);
     response.msg = 'no config founded for requested data';
     res.json(response);
     return;
  }
  // else request data base for data : 
  // get collection name : 
  var collectionName = listConfObject.collection
  console.log('collection ' + collectionName)
  var insert = req.body;
  var query = {}
  query.name = insert.name;
  MongoClient.connect(DbUrl, function(err, db) {
    if(err) { 
      console.dir(err);
      res.status(500);
      response.msg = 'Error in server try later';
      res.json(response);
      }

    var collection = db.collection(collectionName);
    collection.remove(query,function(err, success) {
      if (err){
            console.dir(err);
            res.status(501);
            response.msg = 'Cannot find data, try again or contact your admin';
            res.json(response).end();
            return;
      }
      res.status(200);
      response.msg = 'data removed with success !!';
      res.json(response).end();
      db.close();
    });    
  });
})


//for getting a list
router.post("/:idList",function(req,res,next){
  var idList = req.params.idList;
  var response = {}
  var exist = false
  console.log('id list to get from conf' + idList)
  var listConfObject = {}
  // search for this id in listConf FILE :
  for (let index = 0; index < listConfArray.length; index++) {
    console.log('current url '+ listConfArray[index].url )
    if (listConfArray[index].url == idList){
      listConfObject = listConfArray[index];
      exist = true
      break;
    }
  }
  // if conf not found the return req with data not found : 
  if (!exist){
     res.status(201);
     response.msg = 'no config founded for requested data';
     res.json(response);
     return;
  }
  // else request data base for data : 
  // get collection name : 
  var collectionName = listConfObject.collection
  console.log('fields '+ listConfObject.fields)
  var fieldsToreturn = getFieldsToInclude(listConfObject.fields);
  console.log('fields to include '+ listConfObject.fields )
  console.log('fields to include '+ JSON.stringify(fieldsToreturn) )
  var query = req.body;

  // open new connexion (i have to do it because mongoose require schema and in my case it's dynamique !!!)
  MongoClient.connect(DbUrl, function(err, db) {
      if(err) { 
        console.dir(err);
        res.status(500);
        response.msg = 'Error in server try later';
        res.json(response);
        }

      var collection = db.collection(collectionName);

      collection.find({},fieldsToreturn).toArray(function(err, arrayResult) {
            if(err){
              console.dir(err);
              res.status(501);
              response.msg = 'Cannot find data, try again or contact your admin';
              res.json(response);
            }
            res.json(arrayResult).end();
      });    
  });
})

var getFieldsToInclude = function(fields){
  var includedField = {};
  includedField._id = 0;
  for (let index = 0; index < fields.length; index++) {
    const element = fields[index];
    includedField[element] = 1;
  }
  return includedField;
}

module.exports = router