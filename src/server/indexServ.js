var express = require('express')
var courses_router = require('./routers/courses')
var users_router = require('./routers/users')
var mongoose = require('mongoose')
var body_parser = require('body-parser')
var passport = require("passport")
var cookieParser = require("cookie-parser")
var session = require("express-session")
var passport_setup = require("./passport-setup")


var app = express()

var myLogger = function (req,res,next){
	req.date_time = Date.now()
	console.log("another fucking call")
	next()
}


// connect to data-base : 
mongoose.connect('mongodb://localhost/university');
passport_setup();




app.use(body_parser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
		secret: "TKRv0IJs=HYqrvagQ#&!F!%V]Ww/4KiVs$s,<<MX",
		resave: true,
		saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(myLogger)
app.use(express.json())

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res, next) {
  var response = req.date_time
  next()
})


app.get('/gen_random/:min-:max', function (req,res,next){
   var min = parseInt(req.params.min)
   var max = parseInt(req.params.max)
   
   if (isNaN(min) || isNaN(max)) {
       res.status(400)
       res.json({ err : 'Bad params'})
       return;
   }
   
   var result = Math.round((Math.random() * (max - min)) + min);
   res.json({ result: result });

})

app.use('/courses',courses_router)
app.use('/users',users_router)



// for test purpose :
//serve static file :
app.use('/', express.static('test1'));

app.listen(9002);
