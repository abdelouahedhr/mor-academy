import { APP_CONTAINER_CLASS, STATIC_PATH, WDS_PORT, isProd, APP_NAME } from '../shared/config'

const renderFullPage = (html, st) =>
`<!DOCTYPE html>
<html>
    <head>
        <title> ${APP_NAME} </title>
        <link rel="icon" type="image/x-icon" href="${STATIC_PATH}/public/assets/favicon.ico" />
        <link rel="shortcut icon" type="image/x-icon" href="${STATIC_PATH}/public/assets/favicon.ico" />
        <link rel="stylesheet" href="${STATIC_PATH}/css/fontello.css">
        <link rel="stylesheet" href="${STATIC_PATH}/css/styles.css">
    </head>
    <body >
        <div id="${APP_CONTAINER_CLASS}">
            ${html}
        </div>
        <script>
            window.__PRELOADED_STATE__ = ${JSON.stringify(st.getState())}
        </script>
        <script src="${isProd ? STATIC_PATH : `http://localhost:${WDS_PORT}/dist`}/js/bundle.js"></script>
    </body>
</html>`

export default renderFullPage


//