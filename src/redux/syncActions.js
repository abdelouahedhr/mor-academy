import { ADD_CHAPTER, ADMIN_SELECT_SECTION, SIGN_CLICKED } from "./actionsNames"

export const addChapter = (chapter) => {
    return {
        type : ADD_CHAPTER,
        chapter
    }
}

export const adminSection = (section) => {
    return {
        type : ADMIN_SELECT_SECTION,
        data : section
    }
}

export const signClick = (type) => {
    return {
        type : SIGN_CLICKED,
        signtype : type
    }
}