import { PUSH_COURSE_START,
         PUSH_COURSE_SUCCEEDED, 
         PUSH_COURSE_FAILED, 
         FETCH_SIGN_BEGIN, 
         FETCH_SIGN_SUCCES,
         FETCH_SIGN_FAILED } from "./actionsNames"

export function pushCourse(api,data) {
    return (dispatch) => {
        dispatch( { type : PUSH_COURSE_START , body : true } )

        fetch(api)
            .then((response) => {
                if (!response.ok) {
                    dispatch( { type : PUSH_COURSE_FAILED, body :  response.statusText} )
                    throw Error(response.statusText)
                }

                dispatch( { type : PUSH_COURSE_START , body : false } )

                return response
            })
            .then((response) => console.log(response.json()))
            .catch(() =>  dispatch( { type : PUSH_COURSE_FAILED, body : ""} ))
    };
}

export function signin(data, dispatch){
    dispatch( { type : FETCH_SIGN_BEGIN } )
    fetch("http://localhost:8000/users/login", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(data)
              }).then(res => res.json())
              .catch(error => {
                  dispatch( { type : FETCH_SIGN_FAILED , data : error.msg } )
                })
              .then(response => {
                  dispatch( { type : FETCH_SIGN_SUCCES , data : response.msg } )
                })
}

export function signup(data, dispatch){
    dispatch( { type : FETCH_SIGN_BEGIN } )
    fetch("http://localhost:8000/users/sign-up", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(data)
              }).then(res => res.json())
              .catch(error => {
                  dispatch( { type : FETCH_SIGN_FAILED , data : error.msg } )
                })
              .then(response => {
                  dispatch( { type : FETCH_SIGN_SUCCES , data : response.msg || response.err } )
                })
}

export function facebooksign(dispatch){
    dispatch( { type : FETCH_SIGN_BEGIN } )
    fetch("/users/auth/facebook", {
                method: "GET",
                mode: 'cors'
              }).then(res => {console.log(res)})
              .catch(error => {
                  console.log(error)
                  dispatch( { type : FETCH_SIGN_FAILED , data : error.msg } )
                })
              .then(response => {
                  console.log(response)
                  //dispatch( { type : FETCH_SIGN_SUCCES , data : response.msg || response.err } )
                })
}