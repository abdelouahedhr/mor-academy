import { combineReducers } from 'redux'
import { ADD_CHAPTER, 
         ADMIN_SELECT_SECTION, 
         SIGN_CLICKED,
         FETCH_SIGN_BEGIN,
         FETCH_SIGN_SUCCES,
         FETCH_SIGN_FAILED } from "./actionsNames"


function chaptersReducer(state = [], action){
    switch (action.type){
        case ADD_CHAPTER :
            return state.concat(action.chapter)
    }
    return state
}

function coursesReducer(state = [], action){
    switch (action.type){
        case 'FILTER_COURSES' :
            return state.concat(action.filter)
    }
    return state
}

function adminSectionReducer(state = "add-cours", action){
    switch (action.type){
        case ADMIN_SELECT_SECTION :
            return action.data
    }
    return state
}

function signTypeReducer(state = "both", action){
    switch (action.type){
        case SIGN_CLICKED :
            return action.signtype
    }
    return state
}

function signInReducer(state = { phase : "", msg : "" }, action){
    switch (action.type){
        case FETCH_SIGN_BEGIN :
            return {phase : "Loading", msg : "Loading"}
        case FETCH_SIGN_SUCCES :
            return {phase : "SUCCES", msg : action.data}
        case FETCH_SIGN_FAILED :
            return {phase : "FAILED", msg : action.data}
    }
    return state
}

function signUpReducer(state = { phase : "", msg : "" }, action){
    switch (action.type){
        case FETCH_SIGN_BEGIN :
            return {phase : "Loading", msg : "Loading"}
        case FETCH_SIGN_SUCCES :
            return {phase : "SUCCES", msg : action.data}
        case FETCH_SIGN_FAILED :
            return {phase : "FAILED", msg : action.data}
    }
    return state
}

function profileReducer(state = {profile : {},signed : false}, action){
    //if(action.type == FETCH_SIGN_SUCCES)
    return state
}


export default combineReducers({
    chapters : chaptersReducer,
    courses : coursesReducer,
    adminSection : adminSectionReducer,
    signType : signTypeReducer,
    signIn : signInReducer,
    signUp : signUpReducer,
    profile : profileReducer
})