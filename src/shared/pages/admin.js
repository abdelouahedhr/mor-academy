import React, {Component} from "react"


import Navbar from "../reduxContainers/adminNavbar"
import AdminDisplay from "../reduxContainers/adminDisplay"


export default class Coursedisp extends Component{

    render(){
        return (<div id="admin-page">
                    <AdminDisplay />
                    <Navbar />
                </div>)
    }
}