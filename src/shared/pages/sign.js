import React, {Component} from "react"
import SignContainer from "../reduxContainers/signContainer"
import Footer from "../containers/footer"



export default class Sign extends Component{

    render(){
        return (<div className="sign-container">
                    <SignContainer />
                    <Footer style={{top : "0"}}/>
                </div>)
    }
}