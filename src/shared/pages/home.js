import React, {Component} from "react"
import Bg from '../containers/home-section1'
import How from '../containers/home-section2'
import Why from '../containers/home-section3'
import Footer from '../containers/footer'

export default class Home extends Component{

    render(){
        return (
            <div>
                <Bg/>
                <How/>
                <Why/>
                <Footer/>
            </div>)
    }
}

/*<div className="section-bottom" style={{background: "#fcfcfc"}}></div>
                <div className="section-bottom" style={{top : "-80px", background: "linear-gradient(to bottom, #d1d1d1, #f1f1f1, #fcfcfc)"}}></div>
                <div className="section-top"></div>
                <div className="section-bottom" style={{background: "linear-gradient(to bottom, #efededbd, rgba(248, 248, 248, 0.2))"}}></div>
                <div className="section-top" style={{backgroundColor: "rgb(33, 150, 243)", top : "80.5px"}}></div>
                 */

