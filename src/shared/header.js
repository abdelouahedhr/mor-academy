import React, {Component} from "react"
import {NavLink as Link, withRouter} from "react-router-dom"
import SignLink from "./reduxContainers/signLink"
import Profile from "./reduxContainers/profile"
import config from "./appConfig.js"

class Header extends Component{

    constructor(props){
        super(props)
        this.state = {
            color : "transparent",
            shadow : "none"
        }
    }

    componentDidMount(){
        
        if ((window.scrollY > 0 && this.state.shadow != "0 3px 9px rgba(0,0,0,.2)") || !["/","/Home"].includes(this.props.history.location.pathname)){
            this.setState({shadow : "0 3px 9px rgba(0,0,0,.2)"})
        }
        window.addEventListener("scroll", (e) => {
            if(window.scrollY == 0 && this.state.shadow != "none"){
                this.setState({shadow : "none"})
            }else if (window.scrollY > 0 && this.state.shadow != "0 3px 9px rgba(0,0,0,.2)"){
                this.setState({shadow : "0 3px 9px rgba(0,0,0,.2)"})
            }
        })
    }

    componentDidUpdate(){
        if (!["/","/Home"].includes(this.props.history.location.pathname) && this.state.shadow != "0 3px 9px rgba(0,0,0,.2)"){
            this.setState({shadow : "0 3px 9px rgba(0,0,0,.2)"})
        }else if(["/","/Home"].includes(this.props.history.location.pathname) && this.state.shadow != "none" && window.scrollY == 0){
            this.setState({shadow : "none"})
        }
    }

    render(){
        return (
        <header id="header" style={{boxShadow : this.state.shadow}}>
            <Link to="/Home" key="logo" className="logo" replace ></Link>
            <ul className="header-menu">{config.header.map(
                (el)=>{
                        return <Link to={"/"+el.path} activeStyle={{borderBottom: "2px solid white"}}  key={el.name} className="header-item" replace >{el.name}</Link>
                }
            )}
            <SignLink activeStyle={{borderBottom: "2px solid white"}}/>
            </ul>
        </header>)
    }
}

export default withRouter(Header)