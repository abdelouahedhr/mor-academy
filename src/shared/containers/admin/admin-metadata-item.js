import React, {Component} from "react"
import Input from "../../components/input"

export default class MetaDataItem extends Component{

    constructor(props){
        super(props)
        this.state = {
            element : null,
            update : false
        }
    }

    componentDidMount(){
        this.setState({element : this.props.element})
    }


    delete(){
        console.log(this.state.element._id)
        fetch("/list/listCRUD/"+this.props.url,{
            method: "DELETE",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body : JSON.stringify(this.state.element)
        })
        .then(res => res.json())
        .catch(error => {
            console.log(error)
          })
        .then(response => {
            console.log(response)
          })
    }

    update(){
        if(!this.state.update){
            this.setState({update : true})
        }else{
            let element = document.getElementById("element-info"), childs = element.children, metadata = {}, empty = []
            Array.prototype.forEach.call(childs, child =>{
                if((child.tagName == "INPUT" && child.value == "") || (child.tagName == "DIV" && child.className != "editor" && child.children[0].value == "") || (child.className == "editor" && child.children[1].innerHTML == "")){
                    empty.push(child.id)
                }else{
                    if(child.tagName == "INPUT"){
                        metadata[child.name] = child.value
                    }else if(child.tagName == "DIV" && child.className != "editor"){
                        metadata[child.children[0].name] = child.children[0].value
                    }else if(child.tagName == "SPAN"){
                        metadata["image"] = child.children[1].src
                    }else if(child.tagName != "BUTTON"){
                        metadata["body"] = child.children[1].innerHTML
                    }
                }
            })
            if(empty.length == 0){
                metadata._id = this.state.element._id
                fetch("/list/listCRUD/"+this.props.url,{
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body : JSON.stringify(metadata)
                })
                .then(res => res.json())
                .catch(error => {
                    this.setState({update : false})
                  })
                .then(response => {
                    if(response.msg == "data updated with success !!"){
                        this.setState({update : false, element : metadata})
                    }
                  })
                
            }
        }
    }

    render(){
        console.log(this.props, this.state)
        let child
        if(this.state.update){
            child = (<div id="element-info">
                        <Input id="metadata-name" value={this.state.element.name} style={{margin: "20px auto 0 auto", width : "70%"}} class="addcourse-input" type="text" name="name" default="Smitha"/>
                        <Input id="metadata-description" value={this.state.element.description} style={{margin: "20px auto 0 auto", width : "70%"}} class="addcourse-input" type="text" name="description" default="Bdarija"/>
                    </div>)
        }else{
            let data = this.state.element || this.props.element
            child = (<div id="element-info">
                        <span className="metadata-item">{"Smia : "+data.name}</span>
                        <span className="metadata-item">{"B Darija : "+data.description}</span>
                    </div>)
        }
        return (<div
                    className="metadata-element">
                    {child}
                    <div className="metadata-action-container">
                        <span className="metadata-action metadata-delete icon-delete-button" onClick={()=>this.delete()}></span>
                        <span className="metadata-action metadata-update icon-refresh-button" onClick={()=>this.update()} ></span>
                    </div>
                </div>)
    }
}
