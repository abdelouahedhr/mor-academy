import React, {Component} from "react"
import Input from "../../components/input"
import List from "../../components/list"
import Editor from "../../components/moreditor"
import store from "../../../redux/store"
import addChapter from "../../../redux/syncActions"
import ChapterButton from "../../reduxContainers/chapterButton"
import Upload from "../../components/upload"
import config from "../../appConfig"

export default class Courseintro extends Component{

    render(){
        return (<div id="course-chapter">
                    <Input id="course-title" class="addcourse-input input" type="text" name="title" default="Titre?"/>
                    <Input id="course-intro-video" class="addcourse-input input" type="text" name="urlId" default="Lvideo dial l mo9edima?"/>
                    <Editor id="course-chapter-editor" />
                    <ChapterButton />
                </div>)
    }
}