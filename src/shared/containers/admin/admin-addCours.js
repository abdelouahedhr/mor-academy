import React, {Component} from "react"

import CreatedChapters from "../../reduxContainers/createdChapters"
import DefineCourse from "../../reduxContainers/defineCourse"

export default class AdminAddCours extends Component{

    render(){
        return (<div id="add-cours">
                    <DefineCourse />
                    <CreatedChapters />
                </div>)
    }
}