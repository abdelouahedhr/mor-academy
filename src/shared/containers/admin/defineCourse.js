import React, {Component} from "react"
import Intro from "./course-introduction"
import Chapter from "./course-chapter"

export default class defineCourse extends Component{

    render(){
        return (<div id="define-courses">
                    {this.props.chapters == 0 ? <Intro /> : <Chapter />}
                </div>)
    }
}