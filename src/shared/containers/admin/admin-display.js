import React, {Component} from "react"

import AddCours from "./admin-addCours"
import AddUser from "./admin-adduser"
import MetaData from "./admin-metaData"

export default class Admindisp extends Component{

    constructor(props){
        super(props)
        this.state = {
            selected : "add-cours"
        }
    }

    render(){
        let child
        if(this.props.section == "add-cours"){
            child = (<AddCours />)
        }else if(this.props.section == "add-user"){
            child = (<AddUser />)
        }else if(this.props.section == "meta-data-management"){
            child = (<MetaData />)
        }
        return (<div id="admin-display">
                    {
                        child
                    }
                </div>)
    }
}