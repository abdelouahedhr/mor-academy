import React, {Component} from "react"
import Input from "../../components/input"
import List from "../../components/list"
import Editor from "../../components/moreditor"
import ChapterButton from "../../reduxContainers/chapterButton"
import Upload from "../../components/upload"
import config from "../../appConfig"

export default class Courseintro extends Component{

    render(){
        return (<div id="course-intro">
                    <Input id="course-title" class="addcourse-input" type="text" name="title" default="Titre?"/>
                    <List class="addcourse-input select-input" type="drop" dataType={true} url="getDomaines" id="cours-domaine" data={config.domaines} dropClass="fas fa-arrow-circle-down" readOnly="true" name="domaine" placeholder="informatik, lmath, lphisic..."/>
                    <List class="addcourse-input select-input" type="drop" id="cours-difficulty" data={config.difficulty} dropClass="fas fa-arrow-circle-down" readOnly="true" name="difficulty" placeholder="S3oubia"/>
                    <Input id="course-duration" class="addcourse-input" type="text" name="duration" default="Lwe9t?"/>
                    <Input id="course-intro-video" class="addcourse-input" type="text" name="urlId" default="Lvideo dial l mo9edima?"/>
                    <Upload id="course-image-upload" />
                    <Editor id="course-intro-editor" />
                    <ChapterButton />
                </div>)
    }
}