import React, {Component} from "react"
import appConfig from "../../appConfig"

export default class Adminnav extends Component{

    click(e){
        this.props.handleClick(e.target.dataset.name)
    }

    render(){
        return (<div id="admin-navbar">
                    {
                        appConfig.adminactions.map((el)=>{
                                                        return <div 
                                                                    key={el.name}
                                                                    onClick={(e)=>this.click(e)}
                                                                    className="admin-navbar-element">
                                                                    <span 
                                                                        className={"admin-navbar-element-icon icon-"+el.icon} 
                                                                        data-name={el.name} 
                                                                        style={el.name==this.props.adminSection ? {backgroundColor : "#156bae", color : "white", textShadow : "0 2px 42px #0f7ed2e0, 0 2px 42px #0f7ed2e0"} : {}}>
                                                                    </span>
                                                                    <span className="admin-navbar-element-text">{el.description}</span>
                                                                </div>
                                                    })
                    }
                </div>)
    }
}