import React, {Component} from "react"
import CourseButton from "../../reduxContainers/courseButton"

export default class CreatedCourses extends Component{

    render(){
        return (<div id="created-chapters">
                    <div id="created-chapters-top">
                        Chapitrat
                    </div>
                    <div id="created-chapters-body">
                        {
                            this.props.chapters.map((item,index)=><span key={index}>{item.title}</span>)
                        }
                    </div>
                    <CourseButton />
                </div>)
    }
}