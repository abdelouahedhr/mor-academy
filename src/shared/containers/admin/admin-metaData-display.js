import React, {Component} from "react"
import AddMetatData from "./admin-add-metadata"
import MetaDataItem from "./admin-metadata-item"

export default class AddUser extends Component{

    constructor(props){
        super(props)
        this.state = {
            add : false
        }
        this.addClick = this.addClick.bind(this)
    }

    addClick(){
        this.setState({add : true})
    }

    render(){
        let addSection
        if(this.props.selected == null){
            addSection = null
        }else{
            if(!this.state.add){
                addSection = (<button 
                                className="add-icon add-button"
                                onClick={()=>this.addClick()}></button>)
            }else{
                addSection = <AddMetatData click={(e) => this.props.click(e)} />
            }
        }
        return (<div id="data-display">
                    {this.props.data.map((el,index)=>(<MetaDataItem key={index} url={this.props.selected[0].url} element={el} name={el.name} description={el.description}/>))}
                    {addSection}
                </div>)
    }
}