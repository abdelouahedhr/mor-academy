import React, {Component} from "react"
import DataDisplay from "./admin-metaData-display"
import Input from "../../components/input"
import List from "../../components/list"
import Upload from "../../components/upload"
import config from "../../appConfig"

export default class AddUser extends Component{

    constructor(props){
        super(props)
        this.state = {
            selected : null,
            data : []
        }
        this.dataClick = this.dataClick.bind(this)
        this.validateAdd = this.validateAdd.bind(this)
    }

    dataClick(e){
        let selected = config.metaData.filter(el => {return el.description == e.target.innerHTML})
        this.setState({selected : selected})
        fetch("/list/"+selected[0].url,{
            method: 'POST',
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
        .then(res => res.json())
        .catch(error => {
            console.log(error)
          })
        .then(response => {
            this.setState({data : response})
          })

    }

    validateAdd(e){
        let element = document.getElementById("add-metadata"), childs = element.children, metadata = {}, empty = []
        Array.prototype.forEach.call(childs, child =>{
            if((child.tagName == "INPUT" && child.value == "") || (child.tagName == "DIV" && child.className != "editor" && child.children[0].value == "") || (child.className == "editor" && child.children[1].innerHTML == "")){
                empty.push(child.id)
            }else{
                if(child.tagName == "INPUT"){
                    metadata[child.name] = child.value
                }else if(child.tagName == "DIV" && child.className != "editor"){
                    metadata[child.children[0].name] = child.children[0].value
                }else if(child.tagName == "SPAN"){
                    metadata["image"] = child.children[1].src
                }else if(child.tagName != "BUTTON"){
                    metadata["body"] = child.children[1].innerHTML
                }
            }
        })
        if(empty.length == 0){
            console.log(metadata)
            fetch("/list/listCRUD/"+this.state.selected[0].url,{
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body : JSON.stringify(metadata)
            })
            .then(res => res.json())
            .catch(error => {
                console.log(error)
              })
            .then(response => {
                console.log(response)
              })
        }
    }

    render(){
        let data = config.metaData.map( el => el.description)
        console.log(this.state.data)
        return (<div id="metadata-management">
                    <List 
                        class="addcourse-input select-input" 
                        type="drop" 
                        id="cours-domaine" 
                        dataClick={(e)=>this.dataClick(e)} 
                        data={data} 
                        dropClass="fas fa-arrow-circle-down" 
                        readOnly="true" 
                        name="type" 
                        placeholder="Ha Likayn..."/>
                    <DataDisplay click={(e)=>this.validateAdd(e)} selected={this.state.selected} data={this.state.data}/>
                </div>)
    }
}