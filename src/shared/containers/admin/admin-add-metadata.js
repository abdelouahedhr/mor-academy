import React, {Component} from "react"
import Input from "../../components/input"

export default class AddMetatData extends Component{

    render(){
        return (<div id="add-metadata">
                    <Input id="metadata-name" style={{margin: "20px auto 0 auto", width : "70%"}} class="addcourse-input" type="text" name="name" default="Smitha"/>
                    <Input id="metadata-description" style={{margin: "20px auto 0 auto", width : "70%"}} class="addcourse-input" type="text" name="description" default="Bdarija"/>
                    <button 
                            className="add-icon add-button"
                            onClick={(e) => this.props.click(e)}></button>
                </div>)
    }
}