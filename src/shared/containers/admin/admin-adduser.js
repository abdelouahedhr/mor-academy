import React, {Component} from "react"
import Input from "../../components/input"
import List from "../../components/list"
import Editor from "../../components/moreditor"
import ChapterButton from "../../reduxContainers/chapterButton"
import Upload from "../../components/upload"
import config from "../../appConfig"

export default class AddUser extends Component{

    render(){
        return (<div id="add-user">
                    <Input id="user-firstname" class="addcourse-input" type="text" name="first name" default="Smia..."/>
                    <Input id="user-lastname" class="addcourse-input" type="text" name="last name" default="Lknia..."/>
                    <Input id="user-phone" class="addcourse-input" type="tel" name="phone" default="Nmira..."/>
                    <Input id="user-mail" class="addcourse-input" type="email" name="e-mail" default="Lmail..."/>
                    <Input id="user-age" class="addcourse-input" type="email" name="number" default="L3mer..."/>
                    <List class="addcourse-input select-input" dataType={true} url="getProfile" type="drop" id="cours-domaine" data={[]} dropClass="fas fa-arrow-circle-down" readOnly="true" name="type" placeholder="Chkaydir..."/>
                    <Upload id="course-image-upload" />
                    <Editor id="course-intro-editor" />
                    <button></button>
                </div>)
    }
}