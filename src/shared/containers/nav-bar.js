import React, {Component} from "react"
import List from "../components/list"
import Button from "../components/button"

export default class Navbar extends Component{
    render(){
        return (
            <div id="navbar-container">
                <div id="navbar">
                    <h1 className="section-title filter-navbar">Gherbel</h1>
                    <input className="navbar-elem input" type="search" placeholder="Chnou bghiti?"/>
                    <List class="navbar-elem elem-container" type="drop" id="courses-domaine" dataType={true} url="getDomaines" dropClass="fas fa-arrow-circle-down" data={[]} readOnly="true" name="domaine" placeholder="informatik, lmath, lphisic..."/>
                    <List class="navbar-elem elem-container" type="add" id="courses-tag" dropClass="fas fa-plus-circle" data={[]} readOnly={false} name="tags" placeholder="Cheyer 3ellah"/>
                    <Button type="normal" color="#20cb97" value="Gherbel" class="sign-in navbar-elem" style={{height : "35px", width: "160px", display : "inline-block"}} />
                </div>
            </div>
        )
    }
}