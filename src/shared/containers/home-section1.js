import React, {Component} from "react"
import SignIn from "../reduxContainers/signInLink"
import SignUp from "../reduxContainers/signUpLink"


export default class Bg extends Component{

    constructor(props){
        super(props)
        this.state = {
            domaine : "Doumine..."
        }
    }


    render(){
        return (<div id="top-landing-page">
                    <div className="main-landing-page-block">
                        <div className="main-img-text-block">
                            <div className="main-text-block">
                                <h1 className="title">L9RAYA JATTAL 3ENDEK, B DARIJA</h1>
                                <h2 className="sub-title">courat b darija o fabouuur</h2>
                                <div className="search-container">
                                    <input className="main-search-input" type="text" placeholder="Bghit.."/>
                                    <div className="main-search-input-domain-cotainer">
                                        <div className="main-search-input-domain">{this.state.domaine}</div>
                                    </div>
                                    <div className="main-search-input-icon"></div>
                                </div>
                                <div className="signing-container">
                                    <SignIn />
                                    <SignUp />
                                </div>
                            </div>
                            <div className="main-img-block">
                                <img className="main-img" src="./static/public/assets/background2.png"/>
                            </div>
                        </div>  
                    </div>
                </div>)
    }
}
