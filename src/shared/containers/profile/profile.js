import React, {Component} from "react"

export default class List extends Component{

    render(){
        return (<div className="profile-container" >
                            <svg 
                                width="300px" 
                                height="35px" 
                                viewBox="0 0 300 35"
                                preserveAspectRatio="none"
                                >
                                <defs>
                                    <radialGradient id="gradient">
                                        <stop offset="0%" stopColor="rgb(24, 138, 230)"/>
                                        <stop offset="100%" stopColor="rgb(33, 150, 243)"/>
                                    </radialGradient>
                                </defs>
                                <path 
                                    className="profile" 
                                    d={"M0,0 H 300 C300,0 285,0 285,17.5 C285,17.5 285,35 270,35 H 30 C30,35 15,35 15,17.5 C15,17.5 15,0 0,0 Z"} 
                                    stroke="none" 
                                    fill="url(#gradient)" 
                                    vectorEffect="non-scaling-stroke"/>
                            </svg>
                        </div>)
    }
}