import React, {Component} from "react"

export default class SignMsg extends Component{    

    render(){
        
        return(<div className="sign-msg-container">
                    <div className="sign-msg">
                        <span className={"sign-icon "+( this.props.type ? "icon-check" : "icon-failed")}></span>
                        <span className="sign-text">{this.props.text}</span>
                    </div>
                </div>)
    }
}