import React, {Component} from "react"
import { Redirect } from 'react-router'
import SignIn from "./signIn"
import SignUp from "./signUp"
import SignMsg from "./signMsg"



export default class Sign extends Component{

    constructor(props){
        super(props)
        this.state = {
            signup : {
                        result : false,
                        type : false,
                        msg : ""
                    },
            signin : {
                        result : false,
                        type : false,
                        msg : ""
                    }
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.signUpState.phase == "SUCCES" && nextProps.signUpState.msg == "user created with success and mail confirmation sended !!"){
            this.setState({signup : {result : true, type : true, msg : "Tamat l 3amalia bi naja7, chouf wach weslek wa7ed lmail"}, signin : {result : false, type : false, msg : ""}})
        }else if(nextProps.signUpState.phase == "SUCCES" && nextProps.signUpState.msg == "User already exist !!"){
            this.setState({signup : {result : true, type : false, msg : "AHIA!! Rak(i) waaah m9eyda"}, signin : {result : false, type : false, msg : ""} })
        }else if(nextProps.signInState.phase == "SUCCES" && nextProps.signInState.msg == "Authetication with success!!"){
            this.setState({signup : {result : false, type : false, msg : ""}, signin : {result : true, type : true, msg : ""} })
        }
        
    }

    render(){
        let child
        if(this.state.signup.result == true && this.state.signup.type == true){
            child =  (<div className="sign-body">
                        <SignMsg type={true} text={this.state.signup.msg} />
                    </div>)
        }else if(this.state.signup.result == true && this.state.signup.type == false){
            child =  (<div className="sign-body">
                        <SignMsg type={false} text={this.state.signup.msg} />
                    </div>)
        }else if(this.state.signin.result == true && this.state.signin.type == true){
            child =  (<Redirect to="/Courses" />)
        }else if(this.props.signtype == "both"){
            child =  (<div className="sign-body">
                    <SignIn state={this.props.signInState} signin={(data)=>this.props.signin(data)} />
                    <SignUp state={this.props.signUpState} signup={(data)=>this.props.signup(data)} />
                </div>)
        }else if(this.props.signtype == "in"){
            child =(<div className="sign-body">
                    <SignIn state={this.props.signInState} signin={(data)=>this.props.signin(data)} />
                </div>)
        }else{
            child =(<div className="sign-body">
                    <SignUp state={this.props.signUpState} signup={(data)=>this.props.signup(data)} />
                </div>)
        }
        return child
    }
}