import React, {Component} from "react"
import {NavLink as Link} from "react-router-dom"



export default class Sign extends Component{

    click(){
        this.props.handleClick()
    }

    render(){
        return (<Link to="/Sign" activeStyle={{borderBottom: "2px solid white"}} onClick={()=>this.click()} className="header-item" replace >T9eyed</Link>)
    }
}