import React, {Component} from "react"
import Button from "../../components/button"

export default class SignUp extends Component{

    click(e){
        e.preventDefault()
        let form = document.getElementById("sign-up-form"), childs = form.children, user = {}, empty = []
        Array.prototype.forEach.call(childs, child =>{
            if((child.tagName == "INPUT" && child.value == "") || (child.tagName == "DIV" && child.className != "editor" && child.children[0].value == "") || (child.className == "editor" && child.children[1].innerHTML == "")){
                empty.push(child.name)
            }else{
                if(child.tagName == "INPUT"){
                    user[child.name] = child.value
                }else if(child.tagName == "DIV" && child.className != "editor"){
                    user[child.children[0].name] = child.children[0].value
                }else if(child.tagName == "SPAN"){
                    user["image"] = child.children[1].src
                }else if(child.tagName != "BUTTON"){
                    user["body"] = child.children[1].innerHTML
                }
            }
        })
        
        user["username"] = user["e-mail"]
        if(empty.length == 0){
            this.props.signup(user)
        }
    }
    

    render(){
        if(this.props.state.phase == "Loading"){
            return (<div className="sign-up-container">
                    <div className="form-container">
                        <h1 className="section-title" >LAAA!!<br/>DOUZ T9EYED</h1>
                        <form id="sign-up-form" className="sign-form" >
                            <input className="help-input help-us-input"  placeholder="Smia" name="first Name" type="text" />
                            <input className="help-input help-us-input"  placeholder="Lknia" name="Last Name" type="text" />
                            <input className="help-input help-us-input"  placeholder="Semi rassek" name="Username" type="text" />
                            <input className="help-input help-us-input"  placeholder="lmail" name="e-mail" type="email" />
                            <input className="help-input help-us-input"  placeholder="lcoude" name="password" type="password" />
                            <Button type="normal" color="#ededed" click={(e)=>this.click(e)} value="TSENA..." class="validate-chapter-button" style={{boxShadow: "0 2px 32px #aeaeae38", color : "#f8f8f8", backgroundColor : "#ededed"}} />
                        </form>
                    </div>
                </div>)
        }else{
            return (<div className="sign-up-container">
                    <div className="form-container">
                        <h1 className="section-title" >LAAA!!<br/>DOUZ T9EYED</h1>
                        <form id="sign-up-form" className="sign-form" >
                            <input className="help-input help-us-input"  placeholder="Smia" name="first Name" type="text" />
                            <input className="help-input help-us-input"  placeholder="Lknia" name="Last Name" type="text" />
                            <input className="help-input help-us-input"  placeholder="Semi rassek" name="Username" type="text" />
                            <input className="help-input help-us-input"  placeholder="lmail" name="e-mail" type="email" />
                            <input className="help-input help-us-input"  placeholder="lcoude" name="password" type="password" />
                            <Button type="normal" color="#2196f3" click={(e)=>this.click(e)} value="LTA7E9" class="validate-chapter-button" style={{boxShadow: "0 2px 32px #0f7cd238"}} />
                        </form>
                    </div>
                </div>)
        }
       
    }
}