import React, {Component} from "react"
import Button from "../../components/button"
import FbSign from "../../reduxContainers/facebookButton"

export default class SignIn extends Component{

    click(e){
        e.preventDefault()
        let form = document.getElementById("sign-in-form"), childs = form.children, user = {}, empty = []
        Array.prototype.forEach.call(childs, child =>{
            if((child.tagName == "INPUT" && child.value == "") || (child.tagName == "DIV" && child.className != "editor" && child.children[0].value == "") || (child.className == "editor" && child.children[1].innerHTML == "")){
                empty.push(child.name)
            }else{
                if(child.tagName == "INPUT"){
                    user[child.name] = child.value
                }else if(child.tagName == "DIV" && child.className != "editor"){
                    user[child.children[0].name] = child.children[0].value
                }else if(child.tagName == "SPAN"){
                    user["image"] = child.children[1].src
                }
            }
        })
        
        let data = {}
        data["username"] = user["e-mail"]
        data["password"] = user["password"]
        if(empty.length == 0){
            this.props.signin(data)
        }
    }

    render(){
       return (<div className="sign-in-container">
                    <div className="form-container">
                        <h1 className="section-title" >M9eyed</h1>
                        <FbSign/>
                        <form id="sign-in-form" className="sign-form" >
                            <input className="help-input help-us-input"  placeholder="lmail" name="e-mail" type="email" />
                            <input className="help-input help-us-input"  placeholder="lcoude" name="password" type="password" />
                            <Button type="normal" color="#2196f3" click={(e)=>this.click(e)} value="LTA7E9" class="validate-chapter-button" style={{boxShadow: "0 2px 32px #0f7cd238"}} />
                        </form>
                    </div>
                </div>)
    }
}