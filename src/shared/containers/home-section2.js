import React, {Component} from "react"
import Comp from "../components/comp"
import config from "../appConfig"

export default class How extends Component{

    render(){
        return (<div className="home-section">
                    <h1 className="section-title">KIFACH?</h1>
                    <div className="how-section">
                        {config.how.map((el,index)=> {
                                return (<div key={index} className="how-container">
                                            <div className="how-shapeimg-container" style={{background : "url(./static/public/assets/"+el.shape+".jpg) no-repeat center", backgroundSize : "contain"}}>
                                                <div className="how-container-img" style={{background : "url(./static/public/assets/"+el.image+".png) no-repeat center", backgroundSize : "contain"}}></div>
                                            </div>
                                            <div className="how-container-texte">
                                                <div className="how-container-txt">{el.text}</div>
                                                <div className="how-container-description">nc,dc djfrj jdc,dc iodc,cez i,cecxe iee ujfncrfje ujfrfj ufjcdjd idjczidfzefj dfidfz icdcndce iceidcei icdc,ec icic icnen icdnedi icecei cdujez zeizenf idjj</div>
                                            </div>
                                        </div>)
                            
                        })}
                    </div>
                </div>) 
    }
}

/*let howComponents = config.how.map((el)=><Comp key={el.image} type="how" image={el.image} text={el.text} class="how-component"/>)
        return (<div className="home-section">
                    <h1 className="section-title">KIFACH?</h1>
                    <div className="how-container">
                        {howComponents}
                    </div>
                </div>) */