import { connect } from "react-redux"
import FbBtn from "../components/facebookButton"
import { facebooksign } from "../../redux/asyncActions"

const mapStateToProps = state =>{
    return {
        signtype : state.signType
    }
}

const mapDispatchToProps = dispatch => {return {sign : ()=> facebooksign(dispatch)}}


export default connect(mapStateToProps, mapDispatchToProps)(FbBtn)