import { connect } from "react-redux"
import Profile from "../containers/profile/profile"

const mapStateToProps = state =>{
    return {
        Profile : state.profile
    }
}


export default connect(mapStateToProps)(Profile)