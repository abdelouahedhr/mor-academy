import { connect } from "react-redux"
import Admindisp from "../containers/admin/admin-display"

const mapStateToProps = state =>{
    return {
        section : state.adminSection
    }
}

export default connect(mapStateToProps)(Admindisp)