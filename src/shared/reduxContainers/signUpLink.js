import { connect } from "react-redux"
import { signClick } from "../../redux/syncActions"
import SignUpLink from "../components/signUpLink"

const mapStateToProps = state =>{
    return {
        signtype : state.signType
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : ()=> dispatch(signClick("up"))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(SignUpLink)