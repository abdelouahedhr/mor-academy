import { connect } from "react-redux"
import DefineCourse from "../containers/admin/defineCourse"

const mapStateToProps = state =>{
    return {
        chapters : state.chapters
    }
}

export default connect(mapStateToProps)(DefineCourse)