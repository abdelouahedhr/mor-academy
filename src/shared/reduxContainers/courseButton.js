import { connect } from "react-redux"
import { pushCourse } from "../../redux/asyncActions"
import CourseButton from "../components/courseButton"

const mapStateToProps = state =>{
    return {
        chapters : state.chapters
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : (api,data)=> dispatch(pushCourse(api,data))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(CourseButton)