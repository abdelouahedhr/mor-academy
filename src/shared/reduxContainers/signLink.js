import Sign from "../containers/sign/signLink"
import { connect } from "react-redux"
import { signClick } from "../../redux/syncActions"

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : ()=> dispatch(signClick("both"))
    }
}


export default connect(null,mapDispatchToProps)(Sign)