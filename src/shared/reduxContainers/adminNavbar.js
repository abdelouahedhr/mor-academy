import { connect } from "react-redux"
import { adminSection } from "../../redux/syncActions"
import AdminNavbar from "../containers/admin/admin-navbar"

const mapStateToProps = state =>{
    return {
        adminSection : state.adminSection
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : (section)=> dispatch(adminSection(section))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(AdminNavbar)