import { connect } from "react-redux"
import CreatedChapters from "../containers/admin/createdChapters"

const mapStateToProps = state =>{
    return {
        chapters : state.chapters
    }
}

export default connect(mapStateToProps)(CreatedChapters)