import { connect } from "react-redux"
import { signClick } from "../../redux/syncActions"
import SignInLink from "../components/signInLink"

const mapStateToProps = state =>{
    return {
        signtype : state.signType
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : ()=> dispatch(signClick("in"))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(SignInLink)