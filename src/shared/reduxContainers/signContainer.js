import { connect } from "react-redux"
import Sign from "../containers/sign/sign"
import { signin, signup } from "../../redux/asyncActions"

const mapStateToProps = state =>{
    return {
        signtype : state.signType,
        signInState : state.signIn,
        signUpState : state.signUp
    }
}

const mapDispatchToProps = dispatch => {return {
        signin : (data)=> signin(data, dispatch),
        signup : (data)=> signup(data, dispatch)
}}


export default connect(mapStateToProps, mapDispatchToProps)(Sign)