import { connect } from "react-redux"
import { addChapter } from "../../redux/syncActions"
import ChapterButton from "../components/chapterButton"

const mapStateToProps = state =>{
    return {
        chapters : state.chapters
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleClick : (chapter)=> dispatch(addChapter(chapter))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(ChapterButton)