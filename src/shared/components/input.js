import React, {Component} from "react"

export default class Input extends Component{

    render(){
        return <input 
                    id={this.props.id}
                    className={this.props.class+" input " + (this.props.required? "required-input" : "")}
                    type={this.props.type} 
                    name={this.props.name} 
                    style={this.props.style}
                    defaultValue={this.props.value || ""}
                    placeholder={this.props.default}
                />
    }
}