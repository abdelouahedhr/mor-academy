import React, {Component} from "react"

export default class Chapterbutton extends Component{

    constructor(props){
        super(props)
        this.click = this.click.bind(this)
    }

    click(){
        let element = document.getElementById("course-intro") || document.getElementById("course-chapter"), childs = element.children, chapter = {}, empty = []
        Array.prototype.forEach.call(childs, child =>{
            if((child.tagName == "INPUT" && child.value == "") || (child.tagName == "DIV" && child.className != "editor" && child.children[0].value == "") || (child.className == "editor" && child.children[1].innerHTML == "")){
                empty.push(child.id)
            }else{
                if(child.tagName == "INPUT"){
                    chapter[child.name] = child.value
                }else if(child.tagName == "DIV" && child.className != "editor"){
                    chapter[child.children[0].name] = child.children[0].value
                }else if(child.tagName == "SPAN"){
                    chapter["image"] = child.children[1].src
                }else if(child.tagName != "BUTTON"){
                    chapter["body"] = child.children[1].innerHTML
                }
            }
        })
        if(empty.length == 0){
            this.props.handleClick(chapter)
        }
    }

    render(){
        return <button className="validate-chapter-button" value="Salit" onClick={this.click}>Salit</button>
    }
}

