import React, {Component} from "react"

export default class List extends Component{

    constructor(props){
        super(props)
        this.state = {
            value : "",
            drop : "Off",
            active : "Off",
            input : this,
            dataType : false,
            data : [],
            list : []
        }

        this.inputClick = this.inputClick.bind(this)
        this.foucusOut = this.foucusOut.bind(this)
        this.dropClick = this.dropClick.bind(this)
        this.dataClick = this.dataClick.bind(this)
    }

    componentDidMount(){
        let element = document.getElementById(this.props.id+"-input")
        this.setState({input : element})
        if(this.props.dataType){
            this.setState({dataType : true})
            fetch("/list/"+this.props.url,{
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            })
            .then(res => res.json())
            .catch(error => {
                console.log(error)
              })
            .then(response => {
                console.log(response)
                if(response == undefined){
                    this.setState({data : [], list : []})
                }else{
                    let list = response.map((el)=>el.description)
                    this.setState({data : response, list : list})
                }
              })
        }else{
            this.setState({dataType : true, data : this.props.data, list : this.props.data})
        }
    }

    inputClick(e,callBack){
        if(this.state.active == "Off"){
            this.setState({active : "On"})
            this.state.input.focus()
        }    
        if(this.state.drop == "Off"){
            this.setState({drop : "On"})
            this.state.input.focus()
        } 
        if(callBack != undefined){
            callBack()
        }  
    }

    dropClick(){
        if(this.state.drop == "Off"){
            this.setState({drop : "On"})
            this.state.input.focus()
        }else{
            this.setState({drop : "Off"})
            this.state.input.focusOut()
        }
    }

    componentDidUpdate(){
        this.state.input.value = this.state.value
    }

    foucusOut(e){
        setTimeout(()=>{
            if(this.state.input.value ==""){
                this.setState({drop : "Off",active : "Off"})
            }else{
                this.setState({drop : "Off"})
            }
        },200)
    }

    dataClick(e){
        this.setState({drop: 'Off', value : e.target.innerHTML})
        if(this.props.dataClick != undefined){
            this.props.dataClick(e)
        }
    }

    render(){
        return (
                    <div id={this.props.id} className={this.props.class}>
                        <input 
                            id={this.props.id+"-input"}
                            className="select-input"
                            value={this.state.value || ""}
                            type="text"
                            name={this.props.name}
                            readOnly={this.props.readOnly}
                            onClick={(e)=>this.inputClick(e)}
                            onBlur={(e)=>this.foucusOut(e)}
                            placeholder={this.props.placeholder}
                        />
                        <div className={"input-dropdown "+this.props.dropClass+" "+this.props.type+"-icon"} onClick={this.dropClick}></div>
                        
                        <div className="dropdown-menu" style={(this.state.drop == "Off"? {visibility : "hidden", height: "135px", width: "100%"} : {visibility : "visible", height: "135px", width: "100%"})}>
                            <svg 
                                width="100%" 
                                height="100%" 
                                viewBox="0 0 100 135"
                                preserveAspectRatio="none"
                                style={{display : "inline-block", filter: "drop-shadow(0 0px 1px #2c8ad33b)"}}
                                >
                                <path 
                                    className="svg-dropdown" 
                                    d={"M50,0 L 52,5 H 99 C98,5 100,5 100,10 V 130 C100,130 100,135 98,135 H 2 C2,135 0,135 0,130 V 10 C0,10 0,5 2,5 H 48 L 50,0 Z"} 
                                    stroke="none" 
                                    fill="white" 
                                    vectorEffect="non-scaling-stroke"/>
                            </svg>
                            <div className="select-data">
                                {this.state.list.map((item, index)=><span key={index} className="drop-list-element" onClick={(e)=>this.dataClick(e)}>{item}</span>)}
                            </div>
                        </div>
                    </div>)
    }
}