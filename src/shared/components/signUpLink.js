import React, {Component} from "react"
import {NavLink as Link} from "react-router-dom"

export default class Footer extends Component{

    click(){
        this.props.handleClick()
    }

    render(){
         return (<Link to="/Sign" onClick={()=>this.click()} className="sign-up button" replace >DOUZ T9EYED</Link>)
    }
}