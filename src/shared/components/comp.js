import React, {Component} from "react"


export default class Howcomp extends Component{

    render(){
        return (<div className={"component "+this.props.class}>
                    <div className={this.props.type+"-img-container"}>
                        <img className={this.props.type+"-img"} src={"./static/public/assets/"+this.props.type+"/"+this.props.image + ".png"}/>
                    </div>
                    <span className={this.props.type+"-text"} >{this.props.text}</span>
                </div>)
    }
}