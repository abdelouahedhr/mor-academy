import React, {Component} from "react"

export default class Upload extends Component{
    constructor(props){
        super(props)
        this.state = {
            hover : false,
            preview : "",
            files : []
        }
        this.drop = this.drop.bind(this)
        this.dragEnter = this.dragEnter.bind(this)
        this.dragLeave = this.dragLeave.bind(this)
    }

    dragEnter(e){
        e.stopPropagation()
        e.preventDefault()
        this.setState({hover : true})
    }

    dragOver(e){
        e.stopPropagation()
        e.preventDefault()
    }

    dragLeave(e){
        e.stopPropagation()
        e.preventDefault()
        this.setState({hover : false})
    }

    drop(e){
        e.stopPropagation()
        e.preventDefault();
        this.setState({hover : false})
        let event = e, eventTarget = event.target
        if (e.dataTransfer.items.length > 0) {
            let file = this.state.files
            for (let i = 0; i < event.dataTransfer.items.length; i++) {
                if (event.dataTransfer.items[i].kind === 'file') {
                    file.concat(event.dataTransfer.items[i].getAsFile());
                    let newFile = event.dataTransfer.files[i]
                    let reader = new FileReader()
                    reader.onload = () => {
                        console.log("error",reader.result)
                        this.setState({preview : reader.result})
                    }
                    reader.readAsDataURL(newFile)
                }
            }
            this.setState({
                files : file
            })
        } else {
            /*
            // Use DataTransfer interface to access the file(s)
            for (var i = 0; i < e.dataTransfer.files.length; i++) {
            console.log('... file[' + i + '].name = ' + e.dataTransfer.files[i].name);
            }*/
        } 
    }

    render(){
        return <span id={this.props.id} className={"file-upload navbar-elem addcourse-input "+ (this.state.hover? "upload-hover":"")}
                    onDrop={(e)=>this.drop(e)} 
                    onDragOver={(e)=>this.dragOver(e)}
                    onDragEnter={(e)=>this.dragEnter(e)}
                    onDragLeave={(e)=>this.dragLeave(e)}>
                    <input className="fileInput" type="file" style={{visibility : "hidden"}} onChange={(e)=>this.handleUpload(e)}/>
                    {this.state.preview == ""? <span></span> : <img className="upload-preview-image" src={this.state.preview}/>}
                </span>
        
    }
}