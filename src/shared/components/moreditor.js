import React, {Component} from 'react'


export default class Editor extends Component{

    constructor(props){
        super(props)
        this.toolbarClick = this.toolbarClick.bind(this)
        this.focus = this.focus.bind(this)
        this.focusout = this.focusout.bind(this)
        this.click = this.click.bind(this)
        this.state = {
            toolbar : ['undo','redo','separator','forecolor','separator','fontsize','fontplus','fontminus','separator','bold','italic','separator','justifyLeft','justifyRight','justifyCenter','justifyFull','separator','h1','h2','p','separator','indent','outdent','separator','insertUnorderedList','insertOrderedList','separator','table','separator'],
            font: false
        }
    }

    toolbarClick(e){
        let command = e.target.getAttribute('name')
        e.preventDefault()
        console.log(command)
        if (command == 'h1' || command == 'h2' || command == 'p') {
            document.execCommand('formatBlock', false, command);
        }

        if (command == 'forecolor' || command == 'backcolor') {
            document.execCommand($(this).data('command'), false, $(this).data('value'));
        }

        if (command == 'createlink' || command == 'insertimage') {
            url = prompt('Enter the link here: ','http:\/\/');
            document.execCommand($(this).data('command'), false, url);
        }

        if (command == 'fontplus') {
            var num = Math.min(parseFloat(document.queryCommandValue('FontSize')) + 1,7).toString();
            document.execCommand('fontSize', false, num);
        }

        if (command == 'fontminus') {
            var num = Math.max(parseFloat(document.queryCommandValue('FontSize')) - 1,1).toString();
            document.execCommand('fontSize', false, num);
        }

        else{
            document.execCommand(command, false, null);
        }
    }

    focus(){
        this.setState({font : true})
    }

    focusout(){
        setTimeout(()=>{
            this.setState({font : false})
        },100)
    }

    click(e){
        e.target.parentNode.parentNode.children[0].value = e.target.getAttribute('value')
    }

    render(){
        let toolbar = this.state.toolbar.map((el,index)=>{
            if(el == 'separator'){
                return <div key={el+index} className='default1 toolbar-separator'></div>
            }if(el == 'fontsize'){
                let values = []
                for(var i=6;i<73;i+=2){
                    values.push(<span key={i} value={i} className='fontvalue default1' onClick={(e)=> this.click(e)}>{i}</span>)
                }
                return (<div key={el+index} className='default1 editortoolbar-button toolbar-fontsize'>
                            <input className='fontsizevalue' type='number' defaultValue={12} onFocus={this.focus} onBlur={this.focusout}/>
                            <div className='default fontsize-container' style={this.state.font?{visibility : 'visible'}:{visibility : 'hidden'}}>
                                {values}                            
                            </div>
                        </div>)
            }else{
                return <div key={el+index} name={el} className='default1 editortoolbar-button' style={{backgroundImage : "url(./static/public/editor/"+el+".svg)"}} onMouseDown={(e)=>this.toolbarClick(e)}></div>
            }
        })
        return  (<div id={this.props.id} className='editor'>
                    <div className='editor-toolbar'>
                        {toolbar}
                    </div>
                    <div contentEditable className='editor-body'></div>
                </div>)
    }
        
}