import { connect } from "react-redux"
import Form from "../containers/form/form"
import { inputChange, inputMap, clearMap, formFallback, formException } from "../../redux/syncActions"
//import { signin, signup } from "../../redux/asyncActions"

const mapStateToProps = state =>{
    return {
        inputchange : state.input,
        inputmap : state.map
    }
}

const mapDispatchToProps = dispatch => {return {
        change : (data)=> inputChange(data, dispatch),
        clearMap : (data)=> clearMap(dispatch),
        map : (data)=> inputMap(data, dispatch),
        fallback : (data)=> formFallback(data, dispatch),
        exception : (data) => formException(data, dispatch)
}}


export default connect(mapStateToProps, mapDispatchToProps)(Form)