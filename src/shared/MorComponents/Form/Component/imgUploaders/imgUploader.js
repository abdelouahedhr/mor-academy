import React, {Component} from "react"
import UploaderSubPreview from "./subImgUploader"

export default class Upload extends Component{
    constructor(props){
        super(props)
        this.state = {
            hover : false,
            preview : [],
            subPreview :[],
            imgWidth : false,
            imgHeight : false,
            files : []
        }
        this.drop = this.drop.bind(this)
        this.dragEnter = this.dragEnter.bind(this)
        this.dragLeave = this.dragLeave.bind(this)
    }

    componentWillMount(){
        if(this.props.default){
            let subPreview, height, width
            if(this.isArray(this.props.default)){
                subPreview = this.props.default.map(el=>{
                    let img = new Image()
                    img.src = el
                    let maxDim = Math.max(img.width, img.height)
                    let  newWidth, newHeight
                    newWidth = img.width*500/maxDim
                    newHeight = img.height*500/maxDim
                    height = newHeight+"px"
                    width = newWidth+"px"
                    return {value : el, width : newWidth, height : newHeight}
                })
            }else{
                let img = new Image()
                img.src = this.props.default
                let maxDim = Math.max(img.width, img.height)
                let newWidth = img.width*500/maxDim, newHeight = img.height*500/maxDim
                height = newHeight+"px", width = newWidth+"px"
                subPreview = [{value : this.props.default, width : newWidth, height : newHeight}]
            }
            this.setState({imgWidth : width, imgHeight : height, preview : this.state.preview.concat(this.props.default), subPreview : subPreview})
        }else{
            this.setState({imgWidth : this.props.layout.width, imgHeight : this.props.layout.height})
        }
    }

    isArray(obj){
        if (typeof Array.isArray === 'undefined') {
            Array.isArray = function(obj) {
              return Object.prototype.toString.call(obj) === '[object Array]';
            }
        }else{
                return Array.isArray(obj)
            }
    }

    dragEnter(e){
        e.stopPropagation()
        e.preventDefault()
        this.setState({hover : true})
    }

    dragOver(e){
        e.stopPropagation()
        e.preventDefault()
    }

    dragLeave(e){
        e.stopPropagation()
        e.preventDefault()
        this.setState({hover : false})
    }

    drop(e){
        e.stopPropagation()
        e.preventDefault();
        this.setState({hover : false})
        let event = e, eventTarget = event.target
        if (e.dataTransfer.items.length > 0) {
            let file = this.state.files
            for (let i = 0; i < event.dataTransfer.items.length; i++) {
                if (event.dataTransfer.items[i].kind === 'file') {
                    file.concat(event.dataTransfer.items[i].getAsFile());
                    let newFile = event.dataTransfer.files[i]
                    let reader = new FileReader()
                    reader.onload = () => {
                        let img = new Image()
                        img.src = reader.result
                        img.onload = () => {
                            let maxDim = Math.max(img.width, img.height)
                            let newWidth = img.width*500/maxDim, newHeight = img.height*500/maxDim
                            let preview = this.state.preview, subPreview = this.state.subPreview
                            if(this.props.limit == 1){
                                preview = [reader.result], subPreview = [{value : reader.result, width : newWidth, height : newHeight}]
                                this.props.change({formId : this.props.formId, type:"inputChange", data : {name: this.props.name, value : reader.result}})
                            }else{
                                preview = preview.concat(reader.result), subPreview = subPreview.concat({value : reader.result, width : newWidth, height : newHeight})
                                this.props.change({formId : this.props.formId, type:"inputChange",  data : {name: this.props.name, value : this.state.preview.concat(reader.result)}})
                            }
                            
                            this.setState({preview : preview, subPreview : subPreview , imgWidth : newWidth+"px", imgHeight : newHeight+"px"})
                        }
                    }
                    reader.readAsDataURL(newFile)
                }
            }
            this.setState({
                files : file
            })
        }
    }


    render(){
        console.log(this.state)
        let length = this.state.preview.length - 1
        return (<div id={this.props.formId+"-"+this.props.name} style={this.props.style} className={"file-upload fileupload-"+this.props.id + (this.state.hover? " upload-hover":"")}
                    onDrop={(e)=>this.drop(e)} 
                    onDragOver={(e)=>this.dragOver(e)}
                    onDragEnter={(e)=>this.dragEnter(e)}
                    onDragLeave={(e)=>this.dragLeave(e)}>
                    <h1 className="main-title" style={{fontSize : "25px", marginLeft : "20px", marginTop : "50px"}}>{this.props.title}</h1>
                    <div className="upload-container" style={{display : "flex"}}>
                        <div className="img-upload-container">
                            <input className="fileInput" type="file" style={{visibility : "hidden"}} onChange={(e)=>this.handleUpload(e)}/>
                            <div className="preview-container" style={this.state.imgWidth? {position : "relative",left : "calc(calc(-"+this.state.imgWidth+" - 600px)/2)"} : {}}>
                                <div className="main-preview" style={{height : this.state.imgHeight ? this.state.imgHeight : null , width : this.state.imgWidth? this.state.imgWidth : null}}>
                                    <img className="upload-preview-image" src={this.state.preview[length] || "none"} style={this.state.imgWidth? {height : "calc("+this.state.imgHeight+" - 20px)", width : "calc("+this.state.imgWidth+" - 20px)"} : {}}/>
                                </div>
                            </div>
                        </div>
                        <UploaderSubPreview preview={this.state.subPreview} limit={this.props.limit} />
                    </div>
                </div>)
    }
}
