import React, {Component} from "react"


export default class SubPreview extends Component{


    constructor(props){
        super(props)
        this.state = {
            number : 0,
            article : null,
            actif : 0
        }
    }

    componentWillReceiveProps(){
        this.setState({number : this.props.preview.length})
    }

    componentDidMount(){
        this.setState({number : this.props.preview.length})
    }

    leftCLick(){
        if(this.state.actif > 0){
            this.setState({actif : this.state.actif-1})
        }
    }

    rightCLick(){
        if(this.state.actif < this.state.number - 1){
            this.setState({actif : this.state.actif+1})
        }       
    }

    render(){
       let child
       if(this.props.preview.length == 0){
           child = <div className="sub-preview-container"><span className="empty-sub-preview">Aucun element ajouté</span></div>
       }else{
            child = ((<div className="sub-preview-container">
                        <div className="left-arrow" onClick={()=>this.leftCLick()} style={{opacity : (this.props.limit == 1 || this.state.actif == 0)? "0.2" : "1"} }></div>
                        <div className="sub-preview" style={{justifyContent : ((this.props.limit == 1 )? "space-evenly" : "start")}}>
                                {this.props.preview.map((el,index)=>{
                                    return <img key={index} className="upload-sub-preview-image"  style={{position : "relative",left : -(el.width? (el.width + 80) : 120)*this.state.actif+"px", width : el.width+"px", height : el.height+"px"}} src={el.value}/>
                                })}
                        </div>
                        <div className="right-arrow" onClick={()=>this.rightCLick()} style={{opacity : (this.props.limit == 1 || (this.state.actif == (this.state.number - 1)))? "0.2" : "1"} }></div>
                    </div>))
       }
        return child
    }
}
