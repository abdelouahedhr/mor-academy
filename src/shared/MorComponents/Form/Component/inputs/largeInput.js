import React, {Component} from "react"

export default class LargeInput extends Component{

    constructor(props){
        super(props)
        this.state = {
            value : ""
        }
    }

    change(e){
        let value = e.target.value
        this.setState({value : value})
        if(this.props.change){
            this.props.change({formId : this.props.formId, type:"inputChange", data : {name : this.props.params.name, value : e.target.value}})
        }
    }

    render(){
        return (<label id={this.props.id}  style={{minHeight : "105px", height : "auto"}} htmlFor={this.props.id+"-input"} aria-label={this.props.params.name} className={"input-container " + ((this.props.required && this.state.value == "")? "required-input" : "")}>
                    <textarea 
                        id={this.props.id+"-input"}
                        className=" input"
                        defaultValue={this.props.params.value || ""}
                        name={this.props.params.name} 
                        onChange={(e)=>this.change(e)}
                        style={{minHeight : "105px", height: this.props.params.rows*20 + "px", lineHeight : "35px", ...this.props.style}}
                        rows={this.props.params.rows}
                        placeholder={this.props.params.placeholder}
                    />
                </label>)
    }
}