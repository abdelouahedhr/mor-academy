import React, {Component} from "react"
import {NavLink as Link, withRouter} from "react-router-dom"

class LinkInput extends Component{

    constructor(props){
        super(props)
        this.state = {
            
        }
    }

    render(){
        return (<Link id={this.props.id} to={"/Sign/ForgottenPassword"} className="input-link">&#10149; {this.props.params.text}</Link>)
    }
}

export default withRouter(LinkInput)