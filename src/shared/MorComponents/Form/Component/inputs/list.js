import React, {Component} from "react"

export default class List extends Component{

    constructor(props){
        super(props)
        this.state = {
            value : "",
            selected : {},
            isLoading : false,
            drop : false,
            error : false,
            data : [],
            brutData : [],
            mapping : {}
        }
        this._isMounted = false

        this.dataClick = this.dataClick.bind(this)
        this.fetchData = this.fetchData.bind(this)
        this.show = this.show.bind(this)
        this.hide = this.hide.bind(this)
    }

    componentDidMount(){
        this._isMounted = true
        let subbed = false, mapped = false, mapping = {}
        if(this.props.mapped !== undefined){
            mapped = true
            mapping = this.props.mapped
            subbed = (this.props.mapped.type == "sub")
        }
        this.setState({ mapped : mapped, subbed : subbed, mapping : mapping})
        if(this.props.params.dataType == "false"){
            if(this.props.params.default){
                this.setState({data : this.props.params.data ,brutData : this.props.params.data, value : this.props.params.default})
            }else{
                this.setState({data : this.props.params.data ,brutData : this.props.params.data})
            }
        }
        if(this.props.params.dataType == "true" && this.state.brutData.length == 0){
            this.fetchData()
        }
    }

    componentWillUnmount(){
        this._isMounted = false
        document.removeEventListener("click", this.hide)
    }

    show(e){
        if(this.state.brutData.length == 0 && this.props.params.dataType == "true" && !this.state.isLoading){
            this.fetchData()
        }
        this.setState({ drop : true })
        document.addEventListener("click", this.hide)
    }

    hide(){
        this.setState({ drop : false })
        document.removeEventListener("click", this.hide)
    }

    isArray(obj){
        if (typeof Array.isArray === 'undefined') {
            Array.isArray = function(obj) {
              return Object.prototype.toString.call(obj) === '[object Array]';
            }
        }else{
                return Array.isArray(obj)
            }
    }

    fetchData(){
        this.setState({isLoading : true})
        fetch(this.props.params.path,{
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        }).then(res => res.json())
        .catch(error => {
            console.log(error)
          })
        .then(response => {
            console.log(response)
            if(response && this.isArray(response)){
                let data = response.map(el => el.name)
                if(this.props.params.default){
                    let defaultValue = response.filter(el=> el._id != this.props.params.default)[0].name
                    this._isMounted && this.setState({data : data, brutData : response, isLoading : false, error : false, value : defaultValue})
                }else{
                    this._isMounted && this.setState({data : data, brutData : response, isLoading : false, error : false})
                }
            }else{
                console.log("someting wrong happened : ", response)
                this._isMounted && this.setState({isLoading : false, error : true})
            }
            
          })
    }

    dataClick(e, item){
        e.preventDefault()
        e.stopPropagation()
        let value, selected
        if(this.props.params.dataType == "true"){
            selected = this.state.brutData.filter(el => el.name == item)[0]
            value = selected._id
            this.setState({selected : selected, value : item})
        }else{
            value = item
            this.setState({selected : item, value : item})
        }
        
        if(this.props.dataClick){
            
            this.props.dataClick(e)
        }

        if(this.state.mapping.mappingEvent == "dataClick" && this.state.mapping.condition(selected)){
            this.props.change({formId : this.props.formId, type: "inputChange", data : {name : this.props.params.name, value : value}})
            this.props.map({newAttribute : this.state.mapping.change(selected),mapping : this.state.mapping})
        }else if(this.props.change){
            this.props.change({formId : this.props.formId, type: "inputChange", data : {name : this.props.params.name, value : value}})
        }
    }

    map(e){
        e.preventDefault()
        e.stopPropagation()
        this.props.change({formId : this.props.formId})
        this.props.map(this.props.params)
    }

    render(){
        return (<label id={this.props.id} htmlFor={this.props.id+"-input"} onClick={e=>e.stopPropagation()} aria-label={this.props.params.name} className={((this.props.required && this.state.value == "")? "required-input " : "") + (this.state.subbed? "subbed-input" : "list-container")}>
                    <input 
                            id={this.props.id+"-input"}
                            className={this.state.subbed? "select-input subbed-select-input" : "select-input"}
                            value={this.state.value || ""}
                            type="text"
                            name={this.props.params.name}
                            readOnly={this.props.params.readOnly}
                            onClick={(e)=>this.show(e)} 
                            readOnly="readonly"
                            placeholder={this.props.params.placeholder}
                        />
                    <div></div>
                    <div className={"input-dropdown "+this.props.params.type+"-icon icon-"+this.props.params.type} onClick={(e)=>this.show(e)}></div>
                    {this.state.mapping.type == "sub"? <div className={"map-button icon-"+this.state.mapping.icon} onClick={(e)=>this.map(e)}><span className="mapping-message">{this.state.mapping.message}</span></div> : null}
                    <div className="dropdown-menu" style={(this.state.drop? {visibility : "visible"} : {visibility : "hidden"})}>
                        <svg 
                                    width="100%" 
                                    height="100%" 
                                    viewBox="0 0 100 135"
                                    preserveAspectRatio="none"
                                    style={{display : "inline-block", filter: "drop-shadow(0 0 25px rgba(0,0,0,.471))", flexShrink : "0"}}
                                    >
                                    <defs>
                                        <radialGradient spreadMethod="reflect" id="whiteGradient">
                                            <stop offset="0%" stopColor="hsla(0,0%,100%,.012)"/>
                                            <stop offset="100%" stopColor="hsla(0,0%,100%,.059)"/>
                                        </radialGradient>
                                    </defs>
                                    <path 
                                    className="svg-dropdown" 
                                    d={"M50,0 L 52,5 H 99 C98,5 100,5 100,10 V 130 C100,130 100,135 98,135 H 2 C2,135 0,135 0,130 V 10 C0,10 0,5 2,5 H 48 L 50,0 Z"} 
                                    stroke="#6a5b2f2b" 
                                    strokeWidth="3px"
                                    fill="url(#whiteGradient)"
                                    vectorEffect="non-scaling-stroke"/>
                        </svg>
                        <div className="select-data">
                                {
                                    this.state.isLoading 
                                    ? <div className="icon-spinner animate-spin" style={{color : "#c0a671",  fontSize : "35px", margin : "auto"}}></div> 
                                    : this.state.error
                                        ? <div style={{margin : "auto",textAlign : "center", color : "darkgray", fontSize : "20px", textShadow : "0 0 15px #000", fontFamily : "mainRegular"}}>Ooops, une erreur est survenue Clickez ici pour rafraichir <div className="icon-reload" onClick={()=>this.fetchData()} style={{color : "#c0a671", fontSize : "35px", margin : "auto"}}></div></div>
                                        : this.state.data.length == 0
                                            ? <div style={{margin : "auto",textAlign : "center", color : "darkgray", fontSize : "20px", textShadow : "0 0 15px #000", fontFamily : "mainRegular"}}>La liste est vide pour le moment</div>
                                            : this.state.data.map((item, index)=><span key={index} className="drop-list-element" onClick={(e)=>this.dataClick(e, item)}>{item}</span>) 
                                }
                        </div>
                    </div>
                    
                </label>)
    }
}