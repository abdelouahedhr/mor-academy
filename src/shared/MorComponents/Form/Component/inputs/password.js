import React, {Component} from "react"

export default class password extends Component{

    constructor(props){
        super(props)
        this.state = {
            type : true,
            value : ""
        }
    }

    change(e){
        let value = e.target.value
        this.setState({value : value})
        if(this.props.change){
            
            this.props.change({formId : this.props.formId, type:"inputChange", data : {name : this.props.params.name, value : e.target.value}})
        }
    }

    show(e){
        this.setState({type : !this.state.type})
    }

    render(){
        return (<label id={this.props.id} htmlFor={this.props.id+"-input"} aria-label={this.props.params.name} className={"input-container " + ((this.props.required && this.state.value == "")? "required-input" : "")}>
                            <input 
                                id={this.props.id+"-input"}
                                className="select-input"
                                type={this.state.type? "password" : "text"} 
                                name="password" 
                                onChange={(e)=>this.change(e)}
                                style={this.props.style}
                                placeholder={this.props.params.placeholder}
                            />
                            <div className={"show-password " + (this.state.type? "icon-eye" : "icon-eye-off")} onClick={(e)=>this.show(e)}></div>
                </label>)
    }
}