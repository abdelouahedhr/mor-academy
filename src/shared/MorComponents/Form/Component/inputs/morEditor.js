import React, {Component} from "react"


export default class Editor extends Component{

    constructor(props){
        super(props)
        this.toolbarClick = this.toolbarClick.bind(this)
        this.focus = this.focus.bind(this)
        this.focusout = this.focusout.bind(this)
        this.click = this.click.bind(this)
        this.state = {
            toolbar : ["undo","redo","separator","forecolor","separator","fontsize","fontplus","fontminus","separator","bold","italic","separator","justifyLeft","justifyRight","justifyCenter","justifyFull","separator","h1","h2","p","separator","indent","outdent","separator","insertUnorderedList","insertOrderedList","separator","table","separator"],
            font: false,
            value : ""
        }
    }

    componentDidMount(){
        document.getElementById(this.props.id+"-editor").addEventListener("input", (e) => {
            let value = e.target.innerHTML
            this.setState({value : value})
            if(this.props.change){
                this.props.change({formId : this.props.formId, type:"inputChange", data : {name : this.props.params.name, value : value}})
            }
        }, false);
        
    }

    toolbarClick(e){
        let command = e.target.getAttribute("name")
        e.preventDefault()
        if (command == "h1" || command == "h2" || command == "p") {
            document.execCommand("formatBlock", false, command);
        }

        if (command == "forecolor" || command == "backcolor") {
            document.execCommand($(this).data("command"), false, $(this).data("value"));
        }

        if (command == "createlink" || command == "insertimage") {
            url = prompt("Enter the link here: ","http:\/\/");
            document.execCommand($(this).data("command"), false, url);
        }

        if (command == "fontplus") {
            var num = Math.min(parseFloat(document.queryCommandValue("FontSize")) + 1,7).toString();
            document.execCommand("fontSize", false, num);
        }

        if (command == "fontminus") {
            var num = Math.max(parseFloat(document.queryCommandValue("FontSize")) - 1,1).toString();
            document.execCommand("fontSize", false, num);
        }

        else{
            document.execCommand(command, false, null);
        }
    }

    focus(){
        this.setState({font : true})
    }

    focusout(){
        setTimeout(()=>{
            this.setState({font : false})
        },100)
    }

    click(e){
        e.target.parentNode.parentNode.children[0].value = e.target.getAttribute("value")
    }

    render(){
        let toolbar = this.state.toolbar.map((el,index)=>{
            if(el == "separator"){
                return <div key={el+index} className="toolbar-separator"></div>
            }if(el == "fontsize"){
                let values = []
                for(var i=6;i<73;i+=2){
                    values.push(<span key={i} value={i} className="fontvalue default1" onClick={(e)=> this.click(e)}>{i}</span>)
                }
                return (<label key={el+index} htmlFor="font-size-drop" className="editortoolbar-button toolbar-fontsize">
                            <input id="font-size-drop" className="fontsizevalue" type="number" defaultValue={12} onFocus={this.focus} onBlur={this.focusout}/>
                            <div className="default fontsize-container" style={this.state.font?{visibility : "visible"}:{visibility : "hidden"}}>
                                {values}                            
                            </div>
                        </label>)
            }else{
                return <div key={el+index} name={el} className={"editortoolbar-button icon-"+el} onMouseDown={(e)=>this.toolbarClick(e)}></div>
            }
        })
        return  (<div id={this.props.id} className={"editor " + ((this.props.required && this.state.value == "")? "required-input" : "")}>
                    <h1 className="editor-title" style={{textAlign : "center", fontSize : "20px", marginTop : "10px"}}>{this.props.params.placeholder}</h1>
                    <div className="editor-toolbar">
                        {toolbar}
                    </div>
                    <div contentEditable id={this.props.id+"-editor"} className="editor-body"dangerouslySetInnerHTML={{__html: this.props.params.default || null}}></div>
                </div>)
    }
        
}