import React, {Component} from "react"
 
export default class Table extends Component{
    constructor(props){
        super(props)
        this.state = {
            header : [],
            body : [],
            line : {},
            data : []
        }
    }

    componentDidMount(){
        let data = []
        this.props.params.data.body.map(el=>{
            let subdata = {}
            subdata["data"] = {}
            this.props.params.data.header.map((headEl,headIndex)=>{
                if(headIndex == 0){
                    console.log(el[headIndex].data)
                    subdata[headEl.data] = el[headIndex].data
                }else{
                    subdata.data[headEl.data] = el[headIndex]
                }
            })
            data.push(subdata)
        })
        this.setState({header : this.props.params.data.header, body : this.props.params.data.body, data : data})
    }

    cell(el,index,subindex){
        let type = this.state.header[subindex].type
        if(subindex == 0){
            return <td key={subindex}>{el.name}</td>
        }else if(type == "checkBox"){
            return <td key={subindex}><div className={"checkBox-container " + (el == "true"? "icon-check" : "icon-check-empty")}><input type="checkbox" onChange={(e)=>this.check(e,index,subindex)}/></div></td>
        }else if(type == "text"){
            return <td key={subindex}><input type="text" /></td>
        }
    }

    check(e,index,subindex){
        let newBody = this.state.body, header = this.state.header, newData = this.state.data
        newBody[index][subindex] = e.target.checked.toString()
        newData[index].data[header[subindex].data] = e.target.checked.toString()
        this.setState({body : newBody, data : newData})
        if(this.props.change){
            this.props.change({formId : this.props.formId, type:"inputChange", data : {name : this.props.params.name, value : newData}})
        }
    }

    render(){
        return (<table id={this.props.id} className={this.props.class}>
                    <thead>
                        <tr>
                            {this.state.header.map((el, index) => <th key={index}>{el.name}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.body.map((el, index)=> <tr key={index}>{el.map((subel, subindex)=>this.cell(subel,index,subindex))}</tr>)
                        }
                    </tbody>
                </table>)
    }
}