import React, {Component} from "react"

export default class Input extends Component{

    constructor(props){
        super(props)
        this.state = {
            value : ""
        }
    }

    change(e){
        let value = e.target.value
        this.setState({value : value})
        if(this.props.change){
            this.props.change({formId : this.props.formId, type:"inputChange", data : {name : this.props.params.name, value : e.target.value}})
        }
    }

    render(){
        return (<label id={this.props.id} htmlFor={this.props.id+"-input"} aria-label={this.props.params.name} className={"input-container " + ((this.props.required && this.state.value == "")? "required-input" : "")}>
                    <input 
                        id={this.props.id+"-input"}
                        className=" input"
                        type={this.props.params.type} 
                        defaultValue={this.props.params.value || this.props.params.default || ""}
                        name={this.props.params.name} 
                        onChange={(e)=>this.change(e)}
                        style={this.props.style}
                        placeholder={this.props.params.placeholder}
                    />
                </label>)
    }
}