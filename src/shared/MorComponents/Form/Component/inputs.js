import React, {Component} from "react"
import Input from "./inputs/basicInput"
import LargeInput from "./inputs/largeInput"
import Password from "./inputs/password"
import List from "./inputs/list"
import TreeList from "./inputs/treeList"
import Editor from "./inputs/morEditor"
import Table from "./inputs/table"
import Link from "./inputs/link"
import TitledTable from "./inputs/titledTable"
//import BagInput from "../../reduxContainers/bagInput"
//import TreeInput from "../../reduxContainers/treeInput"

export default class FormInput extends Component{

    render(){
        let child
        if(this.props.params.comp == "input"){
            child = <Input 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        readOnly={this.props.readOnly || false}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "large-input"){
            child = <LargeInput 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "link"){
            child = <Link 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "password"){
            child = <Password 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "list"){
            child = <List 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }/*else if(this.props.params.comp == "treeList"){
            child = <TreeList 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "bag"){
            child = <BagInput
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }*/else if(this.props.params.comp == "tree"){
            child = <TreeInput
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "editor"){
            child = <Editor 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "table"){
            child = <Table 
                        params={this.props.params} 
                        class="table-form" 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }else if(this.props.params.comp == "titledTable"){
            child = <TitledTable 
                        params={this.props.params} 
                        mapped={this.props.params.mapping} 
                        map={this.props.map} 
                        class="table-form" 
                        required={this.props.required} 
                        change={this.props.change} 
                        formId={this.props.formId}
                        id={this.props.params.name+"-"+this.props.formId} 
                    />
        }
        return child
    }
}