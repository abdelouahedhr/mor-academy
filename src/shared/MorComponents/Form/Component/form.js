import React, {Component} from "react"
import Input from "./inputs"
import ImgUploadersContainer from "./imgUploaderContainer"
import {List} from "immutable"
import {polyfill} from "es6-promise"
import "isomorphic-fetch"

export default class Form extends Component{

    constructor(props){
        super(props)
        this.state = {
            brutInputs : [],
            inputs : [],
            imguploaders : [],
            brutImguploaders : [],
            form : {},
            missingInputs : [],
            brutUrl : ""
        }
    }

    componentWillMount(){
        if(this.props.defaultValues){
            let newForm = this.state.form
            this.props.defaultValues.map(el=>newForm[el.name]=el.value)
            this.setState({form : newForm, inputs : List(this.props.inputs), brutInputs : List(this.props.inputs), imguploaders : List(this.props.imguploaders), brutImguploaders : List(this.props.imguploaders), brutUrl : this.props.url})
        }else{
            this.setState({inputs : List(this.props.inputs), brutInputs : List(this.props.inputs), imguploaders : List(this.props.imguploaders), brutImguploaders : List(this.props.imguploaders), brutUrl : this.props.url})
        }
    }

    componentWillUnmount(){
        if(this.props.clearMap){
            this.props.clearMap()
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.inputchange.formId == this.props.id && nextProps.inputchange.type == "inputChange"){
            let newForm = this.state.form
            newForm[nextProps.inputchange.data.name] = nextProps.inputchange.data.value
            this.setState({form : newForm})
        }
        if(nextProps.inputchange.formId == this.props.id && (nextProps.inputmap != this.props.inputmap) && nextProps.inputmap.mapping !== undefined){
            let mapping = nextProps.inputmap.mapping
            if(mapping.type == "sub"){
                let newInput = nextProps.inputmap.mapping.newInput
                newInput.mapping = nextProps.inputmap.mapping
                newInput.mapping.newInput = nextProps.inputmap
                let newInputs = this.state.inputs.map(el => {
                    if(el.name == newInput.name){
                        return newInput
                    }else{
                        return el
                    }
                })
                this.setState({inputs : List(newInputs)}) 
            }else if(mapping.type == "modify"){
                let targetInput = this.state.inputs.filter(el =>el.name == mapping.inputTarget)[0]
                targetInput.params[mapping.attributeTarget] = nextProps.inputmap.newAttribute
                let newInputs = this.state.inputs.map(el =>{
                    if(el.name == targetInput.name){
                        return targetInput
                    }else{
                        return el
                    }
                })
                this.setState({inputs : List(newInputs)})
            }else if(mapping.type == "add"){
                
                let newState = this.state.inputs.toArray(), index = newState.findIndex(e=> e.name == nextProps.inputmap.mapping.index)
                if(newState[index+1].name == nextProps.inputmap.newAttribute.name){
                    newState.splice(index+1,1,nextProps.inputmap.newAttribute)
                }else{
                    newState.splice(index+1,0,nextProps.inputmap.newAttribute)
                }
                this.setState({inputs : List(newState)})
            }
        }
    }

    click(e){
        e.preventDefault()
        let required = false, missingInputs = []
        missingInputs = this.state.inputs.map(el=>{
            if(el.required == "true" && (this.state.form[el.name] == "" || this.state.form[el.name] == undefined)){
                required = true
                return true
            }else{
                return false
            }
        })
        if(!required){
            let method = this.props.urtType || "POST"
            console.log(method)
            fetch(this.props.url,{
                method: method,
                headers: {
                    "Content-Type": "application/json"
                },
                body : JSON.stringify(this.state.form)
            })
            .then(res => res.json())
            .catch(error => {
                console.log(error)
              })
            .then(response => {
                if(response == undefined){
                    console.log(response, "failed")
                }else{
                    console.log(response)
                    if(response.exception){
                        this.props.exception(response.exception)
                    }
                    if(this.props.validationFallback.type == "redirect" && response.status){
                        let redirection = {type : "redirect"}
                        if(response.redirect){
                            redirection.to = response.redirect
                        }else{
                            redirection.to = this.props.validationFallback.to
                        }
                        this.props.fallback(redirection)
                    }
                }
              })
        }else{
            this.setState({missingInputs : missingInputs.toArray()})
        }
    }

    render(){
        {
            if(this.props.imguploaders){
                return (<form className="img-preview-form form" style={this.props.style} id={this.props.id}>
                            <ImgUploadersContainer imguploaders={this.state.imguploaders} formId={this.props.id} map={this.props.map} change={this.props.change}/>
                            <div className="form-input-container" >
                                {
                                    this.state.inputs.map((el,index) =>{
                                        return <Input key={index} params={el} required={this.state.missingInputs[index]} formId={this.props.id} map={this.props.map} change={this.props.change}/>
                                    })
                                }
                                <button className={"main-button form-validate-button"} onClick={(e)=>{this.click(e)}}></button>
                            </div>
                        </form>)
            }else{
                return (<form className="normal-form form" style={this.props.style} id={this.props.id}>
                            {
                                this.state.inputs.map((el,index) =>{
                                    return <Input key={index} params={el} required={this.state.missingInputs[index]} formId={this.props.id} map={this.props.map} change={this.props.change}/>
                                })
                            }
                            <button className={"main-button form-validate-button"} aria-label="Valider" name="Valider" onClick={(e)=>{this.click(e)}}></button>
                        </form>)
            }
        }
    }
}
