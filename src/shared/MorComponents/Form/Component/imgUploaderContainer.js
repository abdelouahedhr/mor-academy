import React, {Component} from "react"
import ImgUploader from "./imgUploaders/imgUploader"


export default class ArticleSlider extends Component{


    constructor(props){
        super(props)
        this.state = {
            number : 0,
            article : null,
            actif : 0
        }
    }

    componentDidMount(){
        this.setState({number : this.props.imguploaders.length})
    }

    leftCLick(){
        if(this.state.actif > 0){
            this.setState({actif : this.state.actif-1})
        }
    }

    rightCLick(){
        if(this.state.actif < this.state.number - 1){
            this.setState({actif : this.state.actif+1})
        }       
    }

    render(){
        return this.props.imguploaders.map(
            (el,index)=> <ImgUploader key={index} formId={this.props.formId} default={el.default} change={this.props.change} id={this.props.id} limit={el.limit} layout={el.dimensions} name={el.name} title={el.title} change={this.props.change} />
        )
    }
}