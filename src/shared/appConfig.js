const config = {
    "header" : [
        {
            "name" : "Dar",
            "icon" : "school",
            "path" : "Home"
        },
        {
            "name" : "Cours",
            "icon" : "book",
            "path" : "Courses"
        },
        {
            "name" : "7na",
            "icon" : "groupe",
            "path" : "AboutUs"
        },
        {
            "name" : "Lta7e9",
            "icon" : "sign",
            "path" : "Help"
        }
        
    ],
    "how" : [
        {
            "image" : "choose",
            "shape" : "how1-shape",
            "text" : "Khtar l cour li bghiti t3elem lih"
        },
        {
            "image" : "read",
            "shape" : "how2-shape",
            "text" : "Tferej f l videoyat o 9ra lasse9 li m3aha"
        },
        {
            "image" : "cool",
            "shape" : "how3-shape",
            "text" : "9ra b chwia 3lik b darija o fabour"
        }
    ],
    "why" : [
        {
            "image" : "teach",
            "text" : "Tbghi tdir des cours"
        },
        {
            "image" : "translate",
            "text" : "Kat3ref l chel7a o tbghi terjem dorous li 7atin bach t3em l fa2ida"
        },
        {
            "image" : "marketing",
            "text" : "kat3ref l marketing o tbghi t3awen had l mobadara te3ref o tnje7"
        },
        {
            "image" : "admin",
            "text" : "tbghi tekhdem m3ana f tassyir dial l moubadara"
        },
        {
            "image" : "donate",
            "text" : "tbghi tbere3 b l flous l moubadara bach nweliw ndirou videoyat 7sen"
        },
        {
            "image" : "other",
            "text" : "bghiti t3awen f chi èaja khra mre7ba :)"
        }
    ],
    "coursestest" : [
        {
            "image" : "HTML&CSS",
            "title" : "T3elem l HTML o CSS fnhar wa7ed",
            "duration" : "10h",
            "difficulty" : "sahel mahel",
            "url" : "8-AqCaavUfQ"
        }
    ],
    "adminactions" : [
        {
            "name" : "add-cours",
            "description" : "Zid chi cour",
            "path" : "admin/addCours",
            "icon" : "add-cours"
        },
        {
            "name" : "add-user",
            "description" : "Zid chi akh",
            "path" : "admin/addUser",
            "icon" : "add-user"
        },
        {
            "name" : "courses",
            "description" : "Lcourat",
            "path" : "admin/Courses",
            "icon" : "courses"
        },
        {
            "name" : "users",
            "description" : "Likhwan",
            "path" : "admin/users",
            "icon" : "users"
        },
        {
            "name" : "meta-data-management",
            "description" : "Tama3loumit",
            "path" : "admin/metaData",
            "icon" : "data"
        }
    ],
    "metaData" : [
        {
            "name" : "domaines",
            "description" : "Doumine",
            "url" : "getDomaines"
        },
        {
            "name" : "difficulty",
            "description" : "S3oubia",
            "url" : "getDifficulty"
        },
        {
            "name" : "userType",
            "description" : "Naw3 l Ikhwan",
            "url" : "getProfile"
        }

    ],
    "domaines" : [
        "Lmath",
        "Lphysik",
        "Linformatik"
    ],
    "difficulty" : [
        "Sahel mahel",
        "L7ma9 dial chjer"
    ],
    "userType" : [
         "teacher",
         "developper",
         "translater",
    ]
}

export default config