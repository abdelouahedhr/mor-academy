import "babel-polyfill"

import React from "react"
import ReactDOM from "react-dom"
import { hydrate } from "react-dom"

import App from "../shared/app"
import { APP_CONTAINER_SELECTOR, isProd } from "../shared/config"

import { AppContainer } from "react-hot-loader"
import { createStore, applyMiddleware, compose } from "redux"
import { Provider } from "react-redux"
import thunkMiddleware from "redux-thunk"
import reducers from "../redux/store"
import { BrowserRouter } from "react-router-dom"

if(typeof window !== "undefined") {
  const rootEl = document.querySelector(APP_CONTAINER_SELECTOR)

  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = (isProd ? null : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
  const preloadedState = window.__PRELOADED_STATE__

  const store = createStore(reducers,
    preloadedState,
    // eslint-disable-next-line no-underscore-dangle
    composeEnhancers(applyMiddleware(thunkMiddleware)))

  const wrapApp = (AppComponent, reduxStore) =>
  <Provider store={reduxStore}>
    <BrowserRouter>
      <AppContainer>
        <AppComponent />
      </AppContainer>
    </BrowserRouter>
  </Provider>

  hydrate(wrapApp(App, store), rootEl)

  if (module.hot) {
      // flow-disable-next-line
      module.hot.accept("../shared/app", () => {
        // eslint-disable-next-line global-require
        const NextApp = require("../shared/app").default
        hydrate(wrapApp(NextApp, store), rootEl)
      })
    }
}