
import path from "path"
import nodeExternals from "webpack-node-externals"
import { isProd } from "./src/shared/config"


export default {
  entry: {
      app : ["babel-polyfill","./src/server/index.js"]
  },
  output: {
    filename: "server.js",
    path: path.resolve(__dirname, "src/server")
  },
  target: "node",
  node: {
    __dirname: false,
    __filename: false
  },
  module: {
    rules: [
            {
                test: /\.(js|jsx)$/,
                use:  "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1
                        }
                    },
                    {
                        loader : "postcss-loader",
                        options: {
                            config: {
                              path: "./postcss.config.js"
                            }
                          }
                    }
                ]
            },
            {
                test: /\.svg/,
                    use: {
                        loader: "svg-url-loader",
                        options: {}
                    }
            }
    ],
  },
  externals: nodeExternals(),
  devtool: isProd? false : "source-map",
  resolve: {
    extensions: [".js", ".jsx"],
  },
  mode : isProd? "production" : "development",
}
